#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>

int main()
{
  Assimp::Importer importer;

  const aiScene *scene = importer.ReadFile(
    "assets/volumes.dae",
    aiProcess_CalcTangentSpace |
    aiProcess_Triangulate      |
    aiProcess_JoinIdenticalVertices |
    aiProcess_RemoveRedundantMaterials |
    aiProcess_ImproveCacheLocality |
    aiProcess_OptimizeMeshes |
    aiProcess_OptimizeGraph |
    aiProcess_SortByPType);

  if (!scene)
  {
    std::cout << importer.GetErrorString() << std::endl;
    return -1;
  }

  std::cout << "Scene loaded" << std::endl;

  for (uint32_t i=0;i<scene->mNumMaterials;++i)
  {
    aiMaterial *mat = scene->mMaterials[i];
    aiColor3D diffuse;
    mat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);
    std::cout << "Diffuse color of material: r=" << diffuse.r
              << ";g=" << diffuse.g << ";b=" << diffuse.b << std::endl;

  }

  for (uint32_t i=0;i<scene->mNumMeshes;++i)
  {
    aiMesh *mesh = scene->mMeshes[i];
    std::cout << "Mesh name:" << mesh->mName.C_Str() << std::endl;
    std::cout << "Vertex positions:" << std::endl;
    for (uint32_t j=0;j<mesh->mNumVertices;++j)
    {
      aiVector3D pos = mesh->mVertices[j];
      std::cout << "pos:x=" << pos.x << ";y=" << pos.y << ";z=" << pos.z;
      aiVector3D norm = mesh->mNormals[j];
      std::cout << " | norm:x=" << norm.x << ";y=" << norm.y << ";z=" << norm.z << std::endl; 
    }

    std::cout << "Vertex indices:" << std::endl;
    for (uint32_t j=0;j<mesh->mNumFaces;++j)
    {
      aiFace face = mesh->mFaces[j];
      for (uint32_t k=0;k<face.mNumIndices;++k)
      {
        std::cout << face.mIndices[k] << " ";
      }
      std::cout << std::endl;
    }
    std::cout << "texture coordinates:" << std::endl;
    for (uint32_t j=0;j<mesh->GetNumUVChannels();++j)
    {
      std::cout << "Channel " << j << std::endl;
      for (uint32_t k=0;k<mesh->mNumUVComponents[j];++k)
      {
        aiVector3D uv = mesh->mTextureCoords[j][k];
        std::cout << "x=" << uv.x << ";y=" << uv.y << ";z=" << uv.z << std::endl; 
      }
    }
  }

  GLFWwindow *window;

  if (!glfwInit()) return -1;

  window = glfwCreateWindow(800,600, "Test", NULL, NULL);
  if (!window)
  {
    std::cout << "Window couldn't open" << std::endl;
    glfwTerminate();
    return -1;
  }

  glfwMakeContextCurrent(window);

  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
  {
    std::cout << "OpenGL extensions couldn't load" << std::endl;
    return -1;
  }

  while (!glfwWindowShouldClose(window))
  {
    glClearColor(0.0,0.5,1.0,1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  glfwPollEvents();

  return 0;
}