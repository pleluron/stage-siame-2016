#include "../src/matrix_solver.hpp"
#include "../src/image_analysis.hpp"
#include "../src/settings.hpp"
#include "../src/renderer.hpp"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <vector>

#include <opencv2/opencv.hpp>

int main(int argc, char **argv)
{
  if (argc != 2)
  {
    std::cout << "Usage : ./tracker_video settings" << std::endl;
    return -1;
  }

  settings s;
  load_settings(argv[1], s);

  // Input video
  cv::VideoCapture cap(s.video_path);
  if (!cap.isOpened())
  {
    std::cout << "Can't open video file" << std::endl;
    return -1;
  }

  // Retrieve input video info
  float width = cap.get(cv::CAP_PROP_FRAME_WIDTH);
  float height = cap.get(cv::CAP_PROP_FRAME_HEIGHT);
  double fps = cap.get(cv::CAP_PROP_FPS);
  int fourCC = cap.get(cv::CAP_PROP_FOURCC);

  float aspect = width/height;

  // Output video
  cv::Size output_size = cv::Size(s.height*aspect,s.height);
  cv::VideoWriter out;
  out.open(s.output_path, fourCC, fps, output_size);
  if (!out.isOpened())
  {
    std::cout << "Can't open output file" << std::endl;
    return -1;
  }

  if (!glfwInit()) return -1;

  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  glfwWindowHint(GLFW_SAMPLES, s.samples);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow *window = glfwCreateWindow(output_size.width, output_size.height, "tracker_video rendering", NULL, NULL);

  if (!window)
  {
    std::cout << "Window couldn't open" << std::endl;
    glfwTerminate();
    return -1;
  }

  glfwMakeContextCurrent(window);

  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
  {
    std::cout << "OpenGL extensions couldn't load" << std::endl;
    return -1;
  }

  renderer r;
  r.set_frame_size(output_size.width, output_size.height);

  r.init();
  r.set_camera_projection(2.0*s.focal_length*s.crop_factor/s.sensor_height, aspect, 1.0,50.0);
  r.load(s.model_path);

  const float MIN_MARKER_SIZE = 4.0;
  const float MAX_MARKER_SIZE = 50.0;

  bool first_frame = true;
  int frame_count = 0;

  matrix<3,4> view_mat;

  cv::Mat output_img(output_size.height, output_size.width, CV_8UC3);
  glPixelStorei(GL_PACK_ALIGNMENT, (output_img.step&3)?1:4);
  glPixelStorei(GL_PACK_ROW_LENGTH, output_img.step/output_img.elemSize());

  cv::Mat current_img;
  while (cap.read(current_img))
  {
    std::vector<marker> markers = detect_markers(
      current_img, s.marker_threshold, MIN_MARKER_SIZE, MAX_MARKER_SIZE);
    // Temporal filters
    if (!first_frame)
    {
      
    }
    first_frame = false;

    std::cout << markers.size() << " markers" << std::endl;

    // Draw 
    /*cv::Mat draw_image = current_img.clone();
    for (marker m : markers)
    {
      cv::Point p;
      p.x = m.pos.at(0);
      p.y = m.pos.at(1);
      cv::circle(draw_image, p, m.size/2, cv::Scalar(255,0,255),-1);
    }
    */
    // View matrix solver
    try
    {
      view_mat = solve_view(
        s.markers, markers, 
        2*s.focal_length*s.crop_factor/s.sensor_height, 
        aspect, height, s.marker_size);
      std::cout << view_mat.to_string() << std::endl;
    }
    catch (solver_multiple_solutions &e)
    {
      std::cout << e.what() << std::endl;
      for (auto m : e.get_solutions())
      {
        std::cout << m.to_string() << std::endl;
      }
    }
    catch (solver_no_solution &e)
    {
      std::cout << e.what() << std::endl;
    }

    /*
    cv::Mat rescaled;
    resize(draw_image, rescaled, output_size);
    out.write(rescaled);
    */

    // Scene rendering
    r.set_camera_view_matrix(view_mat);
    r.render();
    glfwSwapBuffers(window);
    glReadPixels(0, 0, output_img.cols, output_img.rows, GL_BGR, GL_UNSIGNED_BYTE, output_img.data);
    cv::flip(output_img,output_img, 0);
    out.write(output_img);
    glfwPollEvents();

    frame_count++;
  }
  cap.release();
  out.release();

  return 0;
}