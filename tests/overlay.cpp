#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <chrono>
#include <thread>

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

#include "../src/renderer.hpp"
#include "../src/settings.hpp"

//#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

int main(int argc, char **argv)
{
  GLFWwindow *window;

  if (argc < 4)
  {
    std::cout << "Usage: ./overlay config_file model_file image" << std::endl;
    return 0;
  }

  settings s;
  try
  {
    load_settings(argv[1], s);
  }
  catch (const std::string &e)
  {
    std::cout << e << std::endl;
    return -1;
  }

  // Overlay image load
  int w,h,c;
  uint8_t *img = stbi_load(argv[3],&w, &h, &c, 3);
  c=3;

  if (!glfwInit()) return -1;

  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  glfwWindowHint(GLFW_SAMPLES, s.samples);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  window = glfwCreateWindow(s.width, s.height, "Test", NULL, NULL);
  if (!window)
  {
    std::cout << "Window couldn't open" << std::endl;
    glfwTerminate();
    return -1;
  }

  glfwMakeContextCurrent(window);

  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
  {
    std::cout << "OpenGL extensions couldn't load" << std::endl;
    return -1;
  }

  renderer r;
  r.set_frame_size(s.width, s.height);

  r.init();
  r.load(argv[2]);

  r.set_overlay_img(w, h, c, img);

  float overlay_blend = 0.8;

  std::cout << "Change overlay opacity     : i/k" << std::endl;
  std::cout << "Change field of view angle : o/l" << std::endl;

  bool hold = false;

  while (!glfwWindowShouldClose(window))
  {
    auto start = std::chrono::high_resolution_clock::now();
    glfwPollEvents();
    if (glfwGetKey(window, GLFW_KEY_I))
    {
      if (!hold) overlay_blend = glm::clamp(overlay_blend+0.1, 0.0,1.0);
      hold = true;
    }
    else if (glfwGetKey(window, GLFW_KEY_K))
    {
      if (!hold) overlay_blend = glm::clamp(overlay_blend-0.1, 0.0,1.0);
      hold = true;
    }
    else if (glfwGetKey(window, GLFW_KEY_O))
    {
      if (!hold) s.fovx += 0.1;
      hold = true;
    }
    else if (glfwGetKey(window, GLFW_KEY_L))
    {
      if (!hold) s.fovx -= 0.1;
      hold = true;
    }
    else
    {
      hold = false;
    }

    r.set_camera_fovx(s.fovx);
    r.set_camera_position(s.camera_pos);
    r.set_camera_center(s.camera_lookat);
    r.set_camera_up(s.camera_up);
    r.set_overlay_blend(overlay_blend);
    r.render();

    glfwSwapBuffers(window);
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> elapsed = end-start;
    auto sleep_time = std::chrono::duration<double, std::milli>(1000.0/60.0) - elapsed;
    std::this_thread::sleep_for(sleep_time);
  }

  std::cout << "Fovx = " << s.fovx << std::endl;

  stbi_image_free(img);
  r.destroy();
  glfwTerminate();

  return 0;
}