#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <chrono>
#include <thread>

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

#include "../src/renderer.hpp"
#include "../src/settings.hpp"

int main(int argc, char **argv)
{
  GLFWwindow *window;

  if (argc < 4)
  {
    std::cout << "Usage: ./still_image config_file model_file output_file" << std::endl;
    return 0;
  }

  settings s;
  try
  {
    load_settings(argv[1], s);
  }
  catch (const std::string &e)
  {
    std::cout << e << std::endl;
    return -1;
  }

  if (!glfwInit()) return -1;

  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  glfwWindowHint(GLFW_SAMPLES, s.samples);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  window = glfwCreateWindow(s.width, s.height, "Test", NULL, NULL);
  if (!window)
  {
    std::cout << "Window couldn't open" << std::endl;
    glfwTerminate();
    return -1;
  }

  glfwMakeContextCurrent(window);

  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
  {
    std::cout << "OpenGL extensions couldn't load" << std::endl;
    return -1;
  }

  renderer r;
  r.set_frame_size(s.width, s.height);

  r.set_camera_fovx(s.fovx);
  r.set_camera_position(s.camera_pos);
  r.set_camera_center(s.camera_lookat);
  r.set_camera_up(s.camera_up);
  r.set_lens_distortion((lens_distortion_type)s.distortion_type, s.distortion_coef);

  r.init();

  r.load(argv[2]);

  r.render();
  glfwSwapBuffers(window);

  r.save_last_frame(argv[3]);

  r.destroy();
  glfwTerminate();

  return 0;
}