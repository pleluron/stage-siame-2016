#include "../src/matrix_solver.hpp"

#include <iostream>
#include <vector>

vec<2> transform(float alpha, float aspect, matrix<3,4> view, vec<3> point)
{
  vec<4> p; p.at(0) = point.at(0); p.at(1) = point.at(1); p.at(2) = point.at(2);
  p.at(3) = 1;
  vec<3> view_point = view*p;
  vec<2> res;
  res.at(0) = alpha*view_point.at(0)/(aspect*view_point.at(2));
  res.at(1) = alpha*view_point.at(1)/(view_point.at(2));
  return res;
}

vec<3> random_point()
{
  vec<3> p;
  for (int i=0;i<2;++i) p.at(i) = (rand()%100)/50.0 - 1.0;
  p.at(2) = (rand()%100)/50.0 - 2.0;
  return p;
}

int main(void)
{
  matrix<4,4> test = identity<4>();

  test.at(1,0) = 4;
  test.at(1,2) = -2; 

  std::cout << test.to_string() << std::endl;

  std::cout << inverse(test).to_string() << std::endl;

  return 0;
}