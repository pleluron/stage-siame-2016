#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <chrono>
#include <thread>

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

#include "../src/renderer.hpp"
#include "../src/matrix_solver.hpp"

int main(int argc, char **argv)
{
  GLFWwindow *window;

  int width = 800;
  int height = 600;
  int samples = 4;

  if (argc >= 3)
  {
    width = atoi(argv[1]);
    height = atoi(argv[2]);
  }

  if (argc >= 4) samples = atoi(argv[3]);

  if (!glfwInit()) return -1;

  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  glfwWindowHint(GLFW_SAMPLES, samples);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  window = glfwCreateWindow(width, height, "Test", NULL, NULL);
  if (!window)
  {
    std::cout << "Window couldn't open" << std::endl;
    glfwTerminate();
    return -1;
  }

  glfwMakeContextCurrent(window);

  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
  {
    std::cout << "OpenGL extensions couldn't load" << std::endl;
    return -1;
  }

  renderer r;
  r.set_frame_size(width, height);

  vec<3> pos = vec3(4.5,2.69,1.07);
  float theta = -M_PI/2.0;
  float phi = -M_PI/4.0;

  r.set_camera_projection(2.0, width/(float)height, 0.05,20.0);

  r.init();

  r.load("assets/volumes.dae");

  double frametime_avg = 0;
  int frames = 0;

  const int frame_cycles = 60;

  bool hold = false;
  bool wireframe = false;

  //r.set_lens_distortion_ptlens(0.02706, -0.09683, -0.02057);

  while (!glfwWindowShouldClose(window))
  {
    if (glfwGetKey(window, GLFW_KEY_UP   )) phi   += 0.01f;
    if (glfwGetKey(window, GLFW_KEY_DOWN )) phi   -= 0.01f;
    if (glfwGetKey(window, GLFW_KEY_RIGHT)) theta += 0.01f;
    if (glfwGetKey(window, GLFW_KEY_LEFT )) theta -= 0.01f;

    if (phi > +M_PI/2) phi = +M_PI/2;
    if (phi < -M_PI/2) phi = -M_PI/2;

    vec<3> dir = vec3(cos(phi)*cos(theta),sin(phi),cos(phi)*sin(theta));
    vec<3> right = normalize(cross(dir, vec3(0,1,0)));
    if (glfwGetKey(window, GLFW_KEY_W)) pos += dir  *0.02f;
    if (glfwGetKey(window, GLFW_KEY_S)) pos -= dir  *0.02f;
    if (glfwGetKey(window, GLFW_KEY_D)) pos += right*0.02f;
    if (glfwGetKey(window, GLFW_KEY_A)) pos -= right*0.02f;

    if (glfwGetKey(window, GLFW_KEY_R))
    {
      if (!hold) wireframe = !wireframe;
      hold = true;
    }
    else hold = false;

    r.set_wireframe(wireframe);
    r.set_camera_view_matrix(look_at(pos, pos+dir, vec3(0,1,0)));
    auto start = std::chrono::high_resolution_clock::now();
    r.render();
    glfwSwapBuffers(window);
    glfwPollEvents();
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> elapsed = end-start;
    start = end;
    frametime_avg += elapsed.count();
    frames++;

    if (frames == frame_cycles)
    {
      std::cout << "Frametime: " << frametime_avg/frame_cycles << "ms" << std::endl;
      frames = 0;
      frametime_avg = 0;
    }
    auto sleep_time = std::chrono::duration<double, std::milli>(1000.0/60.0) - elapsed;
    std::this_thread::sleep_for(sleep_time);
  }
  r.destroy();
  glfwTerminate();

  return 0;
}