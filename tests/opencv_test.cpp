#include <opencv2/opencv.hpp>
#include "../src/image_analysis.hpp"

#include <iostream>
#include <vector>

using namespace cv;

int main()
{

  Mat image = imread("cv_test.jpg");
  std::vector<marker> markers = detect_markers(image);

  Mat drawImage = image.clone();

  for (marker m : markers)
  {
    cv::Point p;
    p.x = m.pos.at(0);
    p.y = m.pos.at(1);
    circle(drawImage, p, m.size/2, Scalar(255,0,255),2);
  }

  imwrite("cv_output.jpg", drawImage);
}