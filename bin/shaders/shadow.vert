#version 450 core

layout (location = 0) uniform mat4 mat;

layout (location = 0) in vec3 pos;
layout (location = 4) in mat4 model_mat;

out vec4 pass_position;

void main()
{
  gl_Position = mat*model_mat*vec4(pos, 1.0);
  pass_position = gl_Position;
}