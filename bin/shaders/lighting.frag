#version 450 core

#define DIFFUSE_ID     0
#define SPECULAR_ID    1
#define EMISSIVE_ID    2
#define NORMALS_ID     3
#define SHININESS_ID   4
#define LIGHTMAP_ID    5
#define TEXTURE_COUNT  6

layout (binding  = 0) uniform sampler2D tex[TEXTURE_COUNT];
layout (binding  = 6) uniform sampler2DArray shadows;
layout (location = 8) uniform uint material_id;
layout (location = 9) uniform vec3 view_pos;

#define SHADOW_BIAS 0.0002
#define LIGHT_BLEED_REDUCTION_AMOUNT 0.1

struct dir_light
{
  vec4  diffuse;
  vec4  ambient;
  vec4  specular;
  vec4  direction;
  uvec4 shadow_index;
};

struct point_light
{
  vec4  diffuse;
  vec4  ambient;
  vec4  specular;
  vec4  position;
  uvec4 shadow_index;
  vec4  attenuation;
};

struct spot_light
{
  vec4  diffuse;
  vec4  ambient;
  vec4  specular;
  vec4  position;
  vec4  direction;
  vec4  attenuation;
  float inner_angle;
  float outer_angle;
  uvec2 shadow_index;
};

struct material
{
  vec4 diffuse; // w for opacity
  vec4 specular; // w for shininess
  vec4 ambient; // w for shininess strength
  vec4 emissive;
};

layout (std430, binding = 0) buffer material_
{
  material materials[];
};

layout(std430, binding = 1) buffer dir_lights_
{
  dir_light dir_lights[];
};

layout(std430, binding = 2) buffer point_lights_
{
  point_light point_lights[];
};

layout(std430, binding = 3) buffer spot_lights_
{
  spot_light spot_lights[];
};

layout(std430, binding = 4) buffer shadow_matrices_
{
  mat4 shadow_matrices[];
};

layout (location = 0) in vec3 pass_position;
layout (location = 1) in vec3 pass_normal;
layout (location = 2) in vec2 pass_uv;
layout (location = 3) in vec3 pass_tangent;

out vec4 out_color;

vec3 half_vec(vec3 H, vec3 L)
{
  return normalize(H+L);
}

float lambert(vec3 N, vec3 L)
{
  return clamp(dot(N,L),0.0,1.0);
}

float blinn_phong(vec3 N, vec3 H, float shininess)
{
  return pow(clamp(dot(N,H),0.0,1.0),shininess);
}

float vsm(vec2 moments, float dist)
{
  float p = float(dist <= moments.x);
  float variance = moments.y - (moments.x*moments.x);
  variance = max(variance, SHADOW_BIAS);
  float d = dist - moments.x;
  float p_max = variance / (variance + d*d);
  float lit = max(p, p_max);
  // Light bleeding reduction
  return clamp((lit-LIGHT_BLEED_REDUCTION_AMOUNT)/(1-LIGHT_BLEED_REDUCTION_AMOUNT), 0.0,1.0);
}

void main(void)
{
  vec3  diffuse   = texture(tex[DIFFUSE_ID  ], pass_uv).rgb;
  vec3  normal    = texture(tex[NORMALS_ID  ], pass_uv).rgb;
  vec3  specular  = texture(tex[SPECULAR_ID ], pass_uv).rgb;
  vec3  emissive  = texture(tex[EMISSIVE_ID ], pass_uv).rgb;
  float shininess = texture(tex[SHININESS_ID], pass_uv).r;
  float lightmap  = texture(tex[LIGHTMAP_ID ], pass_uv).r;

  mat3 normal_space = mat3(
    pass_tangent, 
    normalize(cross(pass_tangent, pass_normal)),
    pass_normal);

  vec3 view_dir = normalize(view_pos - pass_position);

  normal = normal_space * (normal*2.0-1.0);

  material m = materials[material_id];

  vec3 diffuse_light = vec3(0.0);
  vec3 spec_light = vec3(0.0);

  shininess = m.specular.w;

  // directional lights
  for (uint i=0;i<dir_lights.length();++i)
  {
    dir_light l = dir_lights[i];
    vec3 dir = -normalize(l.direction.xyz);
    float lamb = lambert(normal, dir);

    diffuse_light += l.diffuse.xyz*lamb;
    spec_light += sign(lamb)*l.specular.xyz*blinn_phong(normal, half_vec(view_dir,dir), shininess);
    // Shadow mapping
    uint shadow_id = l.shadow_index.x;
    if (shadow_id > 0)
    {
      mat4 mat = shadow_matrices[shadow_id-1];
      vec4 pos_ts = mat*vec4(pass_position,1.0); // position in texture space
      pos_ts.xyz /= pos_ts.w;
      pos_ts.xyz = pos_ts.xyz*0.5+0.5;
      // Variance Shadow Mapping
      vec2 moments = texture(shadows, vec3(pos_ts.xy,shadow_id-1)).rg;
      float shadow = vsm(moments, pos_ts.z);
      diffuse_light *= shadow;
      spec_light *= shadow;
    }
  }

  for (uint i=0;i<point_lights.length();++i)
  {
    point_light l = point_lights[i];
    vec3 dist = l.position.xyz - pass_position;
    vec3 dir = normalize(dist);
    float d2 = dot(dist, dist);
    float att = 1.0/(l.attenuation.x+ sqrt(d2)*l.attenuation.y + d2*l.attenuation.z);
    float lamb = lambert(normal, dir);
    diffuse_light += att*l.diffuse.xyz*lamb;
    spec_light += sign(lamb)*att*l.diffuse.xyz*blinn_phong(normal, half_vec(view_dir,dir), shininess);
  }
  
  // spot lights
  for (uint i=0;i<spot_lights.length();++i)
  {
    spot_light l = spot_lights[i];
    vec3 dist = l.position.xyz - pass_position;
    float d2 = dot(dist, dist);
    vec3 dir = normalize(dist);
    float att = 1.0/(l.attenuation.x+ sqrt(d2)*l.attenuation.y + d2*l.attenuation.z);
    float angle = acos(dot(normalize(dist), -l.direction.xyz))*2;
    float intensity = 1.0-(angle-l.inner_angle)/(l.outer_angle-l.inner_angle);
    intensity = clamp(intensity, 0.0, 1.0);
    float lamb = lambert(normal, dir);
    diffuse_light += att*l.diffuse.xyz*intensity*lamb;
    spec_light += sign(lamb)*att*l.diffuse.xyz*intensity*blinn_phong(normal, half_vec(view_dir, dir), shininess);
    uint shadow_id = l.shadow_index.x;
    if (shadow_id > 0)
    {
      mat4 mat = shadow_matrices[shadow_id-1];
      vec4 pos_ts = mat*vec4(pass_position,1.0); // position in texture space
      pos_ts.xyz /= pos_ts.w;
      pos_ts.xyz = pos_ts.xyz*0.5+0.5;
      // Variance Shadow Mapping
      vec2 moments = texture(shadows, vec3(pos_ts.xy,shadow_id-1)).rg;
      float shadow = vsm(moments, pos_ts.z);
      diffuse_light *= shadow;
      spec_light *= shadow;
    }
  }
  
  diffuse *= m.diffuse.xyz * diffuse_light;
  specular *= m.specular.xyz * spec_light;
  emissive += m.emissive.xyz;

  out_color = vec4(diffuse+specular+m.ambient.xyz,1.0);
}