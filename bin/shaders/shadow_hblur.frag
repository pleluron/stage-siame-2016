#version 450 core

layout (binding = 0) uniform sampler2D tex;
out vec2 out_moments;

const vec2 size = 1.0/textureSize(tex,0);

const vec2 dimension = vec2(1.0,0.0);

void main(void)
{
   
  vec2 total_blur = texelFetch(tex, ivec2(gl_FragCoord.xy), 0).rg*0.375;
  for (int i=-1;i<=1;i+=2)
  {
      total_blur += textureLod(
        tex, 
        (vec2(gl_FragCoord.xy)+i*dimension*1.2)*size,
        0).rg*0.3125;
  }
  out_moments = total_blur;
}