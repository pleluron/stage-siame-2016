#version 450 core

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 norm;
layout (location = 2) in vec3 tang;
layout (location = 3) in vec2 uv;
layout (location = 4) in mat4 model_mat;

layout (location = 0) out vec3 pass_position;
layout (location = 1) out vec3 pass_normal;
layout (location = 2) out vec2 pass_uv;
layout (location = 3) out vec3 pass_tangent;

void main()
{
  vec4 world_pos = model_mat*vec4(pos, 1.0);
  pass_position = world_pos.xyz;

  pass_uv = uv;
  pass_normal  = vec3(model_mat*vec4(norm, 0.0));
  pass_tangent = vec3(model_mat*vec4(tang, 0.0));
}