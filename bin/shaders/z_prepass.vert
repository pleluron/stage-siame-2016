#version 450 core

layout (location = 0) in vec3 pos;

layout (location = 0) uniform mat4 proj_mat;
layout (location = 4) uniform mat4 view_mat;

layout (location = 4) in mat4 model_mat;

layout (location =0) out vec3 pass_position;

void main()
{
  pass_position = vec3(model_mat*vec4(pos, 1.0));
}