#version 450 core

/* Stores depth and squared depth for Variance Shadow Mapping */
layout (location = 0) out vec2 depths;

in vec4 pass_position;

void main(void)
{
  float d = (pass_position.z/pass_position.w)*0.5+0.5;
  float dx = dFdx(d);
  float dy = dFdy(d);
  depths = vec2(d,d*d+0.25*(dx*dx+dy*dy));
}