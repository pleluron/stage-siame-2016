#version 450 core

layout (binding = 0) uniform sampler2D tex;

layout (location = 0) uniform float blend;
layout (location = 1) uniform vec2 viewport_size;

out vec4 color;

void main()
{
  color = vec4(textureLod(tex, gl_FragCoord.xy*viewport_size, 0).rgb,blend);
}