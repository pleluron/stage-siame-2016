#include "settings.hpp"

#include <SHAUN/shaun.hpp>
#include <SHAUN/sweeper.hpp>
#include <SHAUN/parser.hpp>

#include <iostream>

const matrix<3,3> right_hand = matrix3(vec3(1,0,0), vec3(0,0,1), vec3(0,-1,0));

int get_distorsion_type(const std::string &s)
{
  if (s == "NONE") return 0;
  if (s == "POLY3") return 1;
  if (s == "POLY5") return 2;
  if (s == "PTLENS") return 3;
}

int get_int(shaun::sweeper sw, int def)
{
  return sw.is_null()?def:sw.value<shaun::number>();
}

float get_float(shaun::sweeper sw, float def)
{
  return sw.is_null()?def:sw.value<shaun::number>();
}

template <int M>
vec<M> get_vec(shaun::sweeper sw, vec<M> v)
{
  if (sw.is_null()) return v;
  shaun::list l = sw.value<shaun::list>();
  if (l.size() != M) throw settings_exception();
  for (size_t i=0;i<l.size();++i)
  {
    v.at(i) = l.at<shaun::number>(i);
  }
  return right_hand*v;
}

vec<3> null_vec3()
{
  vec<3> n; for (int i=0;i<3;++i) n.at(i) = 0;
  return n;
}


void load_settings(const std::string &filename, settings &s)
{
  shaun::parser p;
  shaun::object o;
  try
  {
    o = p.parse_file(filename);
  }
  catch (shaun::parse_error e)
  {
    std::cerr << e << std::endl;
    throw;
  }

  shaun::sweeper sw(&o);

  // 3D model
  auto model = sw("model_path");
  if (model.is_null()) throw settings_exception();
  s.model_path = (std::string)model.value<shaun::string>();

  auto video= sw("video_path");
  if (video.is_null()) throw settings_exception();
  s.video_path = (std::string)video.value<shaun::string>();

  auto output = sw("output_path");
  s.output_path = output.is_null()?
    std::string("output_")+s.video_path:
    (std::string)output.value<shaun::string>();

  s.height = get_int(sw("height"), 720);
  s.samples = get_int(sw("samples"), 8);
  s.camera_pos    = get_vec<3>(sw("camera_pos")   , null_vec3());
  s.camera_lookat = get_vec<3>(sw("camera_lookat"), null_vec3());
  s.camera_up     = get_vec<3>(sw("camera_up")    , null_vec3());
  s.focal_length = get_float(sw("focal_length"), 50);
  s.crop_factor = get_float(sw("crop_factor"), 1);
  s.sensor_height = get_float(sw("sensor_height"),24);

  auto dist = sw("distortion_type");
  s.distortion_type = dist.is_null()?0:get_distorsion_type((std::string)dist.value<shaun::string>());
  if (s.distortion_type == 1)
  {
    s.distortion_coef.at(0) = get_float(sw("k1"),0.0);
  }
  else if (s.distortion_type == 2)
  {
    s.distortion_coef.at(0) = get_float(sw("k1"),0.0);
    s.distortion_coef.at(1) = get_float(sw("k2"),0.0);
  }
  else if (s.distortion_type == 3)
  {
    s.distortion_coef.at(0) = get_float(sw("a"),0.0);
    s.distortion_coef.at(1) = get_float(sw("b"),0.0);
    s.distortion_coef.at(2) = get_float(sw("c"),0.0);
  }

  s.marker_size = get_float(sw("marker_size"), 0.05);
  s.marker_threshold = get_int(sw("marker_threshold"), 128);
  auto markers = sw("markers_pos");
  if (!markers.is_null())
  {
    shaun::list l = markers.value<shaun::list>();
    for (size_t i=0;i<markers.size();++i)
    {
      shaun::list point = l.at<shaun::list>(i);
      vec<3> pos;
      if (point.size() != 3) throw settings_exception();
      for (size_t j=0;j<point.size();++j)
      {
        pos.at(j) = point.at<shaun::number>(j);
      }
      s.markers.push_back(right_hand*pos);
    }
  }
}