#pragma once

#include <glad/glad.h>
#include <string>
#include <map>

class shader
{
public:
  shader();
  void create();
  void source(GLenum shader_type, const std::string &filename);
  void link();
  GLint getUniformLocation(const std::string &name);
  GLuint getUniformBlockIndex(const std::string &name);
  void uniformBlockBinding(GLuint block_index, GLuint block_binding);
  void use();
  void destroy();
  int id();

  class exception : std::exception
  {
  public:
    exception(const std::string &msg)
    {
      msg_ = msg;
    }
    const char* what() const noexcept
    {
      return msg_.c_str();
    }
  private:
    std::string msg_;
  };

private:
  GLuint program_id_;
  std::map<std::string, GLint> uniform_locations_;

};