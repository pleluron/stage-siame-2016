#include "shader.hpp"

#include <glad/glad.h>
#include <fstream>
#include <sstream>
#include <string>

shader::shader() : program_id_(0)
{

}

void shader::create()
{
  program_id_ = glCreateProgram();
}

void shader::source(GLenum shader_type, const std::string &filename)
{
  std::string code;

  try
  {
    std::stringstream sstream;
    {
      std::ifstream stream;
      stream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
      stream.open(filename);
      sstream << stream.rdbuf();
    }
    code = sstream.str();
  }
  catch (std::ifstream::failure e)
  {
    throw exception(std::string("Can't open ") + filename + e.what());
  }

  GLint success;
  GLchar info_log[2048];

  const char *s = code.c_str();

  GLuint shad_id = glCreateShader(shader_type);
  glShaderSource(shad_id, 1, &s, NULL);
  glCompileShader(shad_id);
  glGetShaderiv(shad_id, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    glGetShaderInfoLog(shad_id, sizeof(info_log), NULL, info_log);
    throw exception(std::string("Can't compile ") + filename + " " + info_log);
  }
  glAttachShader(program_id_, shad_id);
}

void shader::link()
{
  GLint success;
  GLchar info_log[2048];

  glLinkProgram(program_id_);
  glGetProgramiv(program_id_, GL_LINK_STATUS, &success);
  if (!success)
  {
    glGetProgramInfoLog(program_id_, sizeof(info_log), NULL, info_log);
    throw exception(std::string("Can't link :") + info_log);
  }

  // Uniform locations retrieval
  GLint active_uniforms;
  glGetProgramiv(program_id_, GL_ACTIVE_UNIFORMS, &active_uniforms);

  for (GLuint i=0;i<(GLuint)active_uniforms;++i)
  {
    const int BUF_SIZE = 1024;
    GLint size;
    GLenum type;
    GLchar name[BUF_SIZE];
    glGetActiveUniform(program_id_, i, BUF_SIZE, NULL, &size, &type, name);
    GLint location = glGetUniformLocation(program_id_, name);
    if (location != -1)
      uniform_locations_.insert(std::make_pair(name, location));
  }
}

void shader::use()
{
  glUseProgram(program_id_);
}

GLint shader::getUniformLocation(const std::string &name)
{
  try
  {
    return uniform_locations_.at(name);
  }
  catch (...)
  {
    return -1;
  }
}

GLuint shader::getUniformBlockIndex(const std::string &name)
{
  return glGetUniformBlockIndex(program_id_, name.c_str());
}

void shader::uniformBlockBinding(GLuint block_index, GLuint block_binding)
{
  return glUniformBlockBinding(program_id_, block_index, block_binding);
}

int shader::id()
{
  return program_id_;
}