#pragma once

#include <glm/glm.hpp>

/*
 * Light illuminating in a general direction from infinity
 */
class dir_light
{
public:
  glm::vec4  diffuse_;
  glm::vec4  ambient_;
  glm::vec4  specular_;
  glm::vec4  direction_;
  glm::uvec4 shadow_index_;
};

/*
 * Light illuminating in all directions from a given point
 * Light intensity attenuates as a function of distance
 */
class point_light
{
public:
  /* light attenuation is computed as : 1/(att.x+d*att.y+d*d*att.z) where 
   * att.x is the constant attenuation
   * att.y is the linear attenuation
   * att.z is the quadratic attenuation
   * att.w is the computed radius
   */
  glm::vec4  diffuse_;
  glm::vec4  ambient_;
  glm::vec4  specular_;
  glm::vec4  position_;
  glm::uvec4 shadow_index_;
  glm::vec4  attenuation_;
};

/*
 * Point light restricted in a cone
 */
class spot_light
{
public:
  /* a spot illuminates in a cone whose angle is defined as two angles for
   * smoothness purposes. the light is at full intensity inside the inner angle
   * and fades linearly when approaching the outer angle
   */
  glm::vec4 diffuse_;
  glm::vec4 ambient_;
  glm::vec4 specular_;
  glm::vec4 position_;
  glm::vec4 direction_;
  glm::vec4 attenuation_;
  float inner_angle_;
  float outer_angle_;
  glm::uvec2 shadow_index_;
};