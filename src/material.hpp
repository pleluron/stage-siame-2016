#pragma once

#include <glm/glm.hpp>



class material
{
public:
  const static int DIFFUSE_ID    = 0;
  const static int SPECULAR_ID   = 1;
  const static int EMISSIVE_ID   = 2;
  const static int NORMALS_ID    = 3;
  const static int SHININESS_ID  = 4;
  const static int LIGHTMAP_ID   = 5;
  const static int TEXTURE_COUNT = 6;

  glm::vec3 diffuse_;
  glm::vec3 specular_;
  glm::vec3 ambient_;
  glm::vec3 emissive_;

  float opacity_;
  float shininess_;
  float shininess_strength_;
  
  // Ids in renderer::textures_
  int texture_ids_[TEXTURE_COUNT];

};