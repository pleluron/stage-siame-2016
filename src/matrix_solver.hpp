#pragma once

#include <exception>
#include <vector>
#include <string>
#include <cstdarg>
#include <cmath>

#include <stdexcept>

/* MATRIX DECLARATION */

// M is the number of rows, N the number of columns
template <size_t M, size_t N>
class matrix
{
public:
  matrix();
  matrix(const matrix<M,N> &m);
  matrix<M,N> &operator=(const matrix<M,N> &m);
  double &at(size_t i, size_t j); // row, column for matrices
  double &at(size_t i); // row for vectors
  const double &at(size_t i, size_t j) const;
  const double &at(size_t i) const;
  matrix<N,1> row(size_t i);
  matrix<M,1> col(size_t j);
  std::string to_string();
  
private:
  // Implementation is column-major
  double values[M*N];
};

/* VECTOR DECLARATION */

template <size_t M>
using vec = matrix<M,1>;

/* EXCEPTION DECLARATION */

class matrix_exception : std::exception {};
class matrix_out_of_bounds : matrix_exception {};
class matrix_no_inverse : matrix_exception
{
public:
  const char *what() const noexcept { return "There is no inverse matrix";}
};

class solver_exception : std::exception {};
class solver_no_solution : solver_exception
{
public:
  const char *what() const noexcept { return "There is no solution for this system";}
};
class solver_multiple_solutions : solver_exception
{
public:
  solver_multiple_solutions(const std::vector<matrix<3,4>> &solutions) { solutions_ = solutions;}
  const char *what() const noexcept { return "There is multiple solutions for this system"; }
  const std::vector<matrix<3,4>> &get_solutions() { return solutions_; }
private:
  std::vector<matrix<3,4>> solutions_;
};

/* TEMPLATED MATRIX FUNCTIONS DEFINITIONS */

template <size_t M>
class determinant
{
public:
  double value(const matrix<M,M> &mat)
  {
    double det = 0.0;
    for (size_t i=0;i<M;++i)
    {
      det += ((int)(i%2)*-2+1) * mat.at(0,i) * determinant<M-1>().value(submatrix(mat, 0, i));
    }
    return det;
  }
};

template <>
class determinant<1>
{
public:
  double value(const matrix<1,1> &mat)
  {
    return mat.at(0,0);
  }
};

template <size_t M, size_t N>
matrix<M,N>::matrix()
{
  for (size_t i=0;i<M*N;++i) values[i] = 0;
}

template <size_t M, size_t N>
matrix<M,N>::matrix(const matrix<M,N> &m)
{
  for (size_t i=0;i<M*N;++i) values[i] = m.values[i];
}

template <size_t M, size_t N>
matrix<M,N> &matrix<M,N>::operator=(const matrix<M,N> &m)
{
  for (size_t i=0;i<M*N;++i) values[i] = m.values[i];
  return *this;
}

template<size_t M>
matrix<M,M> identity()
{
  matrix<M,M> mat;
  for (size_t i=0;i<M;++i) for (size_t j=0;j<M;++j) mat.at(i,j) = (i==j);
  return mat;
}

template <size_t M, size_t N>
inline double &matrix<M,N>::at(size_t i, size_t j)
{ 
  if (i<0 || i>=M || j<0 || j>=N) throw matrix_out_of_bounds(); 
  return values[j*M+i];
}

template <size_t M, size_t N>
inline double &matrix<M,N>::at(size_t i)
{
  if (i<0 || i>=M) throw matrix_out_of_bounds(); 
  return values[i];
}

template <size_t M, size_t N>
inline const double &matrix<M,N>::at(size_t i, size_t j) const
{
  if (i<0 || i>=M || j<0 || j>=N) throw matrix_out_of_bounds(); 
  return values[j*M+i];
}

template <size_t M, size_t N>
inline const double &matrix<M,N>::at(size_t i) const
{
  if (i<0 || i>=M) throw matrix_out_of_bounds(); 
  return values[i];
}

template <size_t M, size_t N>
matrix<N,1> matrix<M,N>::row(size_t i)
{
  vec<N> v;
  for (size_t j=0;j<N;++j)
  {
    v.at(j) = this->at(i,j);
  }
  return v;
}

template <size_t M, size_t N>
matrix<M,1> matrix<M,N>::col(size_t j)
{
  vec<M> v;
  for (size_t i=0;i<M;++i)
  {
    v.at(i) = this->at(i,j);
  }
  return v;
}

template <size_t M, size_t N>
std::string matrix<M,N>::to_string()
{
  std::string str;
  for (size_t i=0;i<M;++i)
  {
    str += (i==0)?"[":" ";
    for (size_t j=0;j<N;++j)
    {
      str += std::to_string(at(i,j)) + " ";
    }
    str += (i==M-1)?"]":"\n";
  }
  return str;
}

template <size_t M, size_t N>
matrix<M-1,N-1> submatrix(const matrix<M,N> &m, size_t i, size_t j)
{
  matrix<M-1,N-1> res;
  for (size_t i1=0;i1<M-1;++i1)
  {
    for (size_t j1=0;j1<N-1;++j1)
    {
      res.at(i1,j1) = m.at(i1+(i1>=i),j1+(j1>=j));
    }
  }
  return res;
}

template <size_t M, size_t N>
matrix<N,M> transpose(const matrix<M,N> &m)
{
  matrix<N,M> mat;
  for (size_t i=0;i<M;++i)
    for (size_t j=0;j<N;++j)
      mat.at(j,i) = m.at(i,j);
  return mat;
}

template <size_t M>
matrix<M,M> inverse(const matrix<M,M> &m)
{
  double det = determinant<M>().value(m);
  if (det == 0.0) throw matrix_no_inverse();

  matrix<M,M> inv;
  for (size_t i=0;i<M;++i)
  {
    for (size_t j=0;j<M;++j)
    {
      inv.at(i,j) = (int((i+j)%2)*-2+1)*determinant<M-1>().value(submatrix(m, j, i)) / det;
    }
  }
  return inv;
}

template <size_t M, size_t N, size_t P>
matrix<M,P> operator*(const matrix<M,N> &left,const matrix<N,P> &right)
{
  matrix<M,P> res;
  for (size_t i=0;i<M;++i)
  {
    for (size_t j=0;j<P;++j)
    {
      res.at(i,j) = 0.0;
      for (size_t k=0;k<N;++k)
        res.at(i,j) += left.at(i,k) * right.at(k, j);
    }
  }
  return res;
}

template <size_t M, size_t N>
matrix<M,N> operator+(const matrix<M,N> &left, const matrix<M,N> &right)
{
  matrix<M,N> res;
  for (size_t i=0;i<M;++i)
    for (size_t j=0;j<N;++j)
      res.at(i,j) = left.at(i,j) + right.at(i,j);
  return res;
}

template <size_t M, size_t N>
matrix<M,N> operator-(const matrix<M,N> &left, const matrix<M,N> &right)
{
  matrix<M,N> res;
  for (size_t i=0;i<M;++i)
    for (size_t j=0;j<N;++j)
      res.at(i,j) = left.at(i,j) - right.at(i,j);
  return res;
}

template <size_t M, size_t N>
matrix<M,N> &operator +=(matrix<M,N> &a, const matrix<M,N> &b)
{
  a = a+b;
  return a;
}

template <size_t M, size_t N>
matrix<M,N> &operator -=(matrix<M,N> &a, const matrix<M,N> &b)
{
  a = a-b;
  return a;
}

template <size_t M, size_t N>
matrix<M,N> operator-(const matrix<M,N> &left)
{
  matrix<M,N> res;
  for (size_t i=0;i<M;++i)
    for (size_t j=0;j<N;++j)
      res.at(i,j) = -left.at(i,j);
  return res;
}

template <size_t M, size_t N>
matrix<M,N> operator*(double left, const matrix<M,N> &right)
{
  matrix<M,N> res;
  for (size_t i=0;i<M;++i)
    for (size_t j=0;j<N;++j)
      res.at(i,j) = left*right.at(i,j);
  return res;
}

template <size_t M, size_t N>
matrix<M,N> operator*(const matrix<M,N> &right, double left)
{
  matrix<M,N> res;
  for (size_t i=0;i<M;++i)
    for (size_t j=0;j<N;++j)
      res.at(i,j) = left*right.at(i,j);
  return res;
}

template<size_t M>
double length(const vec<M> &v)
{
  double s = 0.0;
  for (size_t i=0;i<M;++i)
    s += v.at(i)*v.at(i);
  return sqrt(s);
}

template <size_t M>
vec<M> normalize(const vec<M> &v)
{
  double mag = 1.0/length(v);
  vec<M> r;
  for (size_t i=0;i<M;++i) r.at(i) = v.at(i)*mag;
  return r;
}

vec<3> cross(const vec<3> &left, const vec<3> &right);

template <size_t M>
double dot(const vec<M> &left, const vec<M> &right)
{
  double res = 0.0;
  for (size_t i=0;i<M;++i)
    res += left.at(i)*right.at(i);
  return res;
}

template <size_t M>
bool operator==(const vec<M> &left, const vec<M> &right)
{
  for (size_t i=0;i<M;++i) if (left.at(i)!=right.at(i)) return false;
  return true;
}

vec<2> vec2(double v1, double v2);
vec<3> vec3(double v1, double v2, double v3);
vec<4> vec4(double v1, double v2, double v3, double v4);

matrix<3,3> matrix3(const vec<3> &p1, const vec<3> &p2, const vec<3> &p3);
matrix<3,4> matrix3x4(const matrix<3,3> &m, const vec<3> &v);

/* SOLVER DECLARATIONS */

matrix<3,4> look_at(vec<3> eye_pos, vec<3> eye_center, vec<3> up);
matrix<4,4> perspective(double alpha, double aspect, double near, double far);

/**
 * Returns the alpha parameter to build perspective matrices given points in 
 * view space and in ndc space
 * The near and far planes are things only present in 3D rendering thus the need
 * to supply them
 * @param view_matrix transforms world points to view points
 * @param world_space the list of world-space points
 * @param ndc_points the list of image-space points
 * @param aspect the ratio width/height of the image 
 * @param deviation maximum authorized deviation to reject incorrect point pairs
 * @returns the alpha parameter for perspective creation
 */
float solve_perspective(
  matrix<3,4> view_matrix,
  std::vector<vec<3>> world_points,
  std::vector<vec<2>> ndc_points,
  float aspect,
  float deviation=0.01);

/** Ellipse on the screen */
typedef struct
{
  vec<2> pos;
  double size; // major axis
  double angle;
} marker;

matrix<3,4> solve_view(
  std::vector<vec<3>> world_points,
  std::vector<marker> ndc_points,
  float alpha,
  float aspect,
  int height,
  float marker_size);