#include "renderer.hpp"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <map>
#include <utility>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

typedef struct
{
  glm::vec3 pos  = glm::vec3(0,0,0);
  glm::vec3 norm = glm::vec3(0,0,1); // normal (normalized)
  glm::vec3 tang = glm::vec3(1,0,0); // tangent (normalized)
  glm::vec2 uv   = glm::vec2(0,0); // texture coordinates
} vertex;

typedef struct
{
  glm::mat4 transform;
  uint mesh_id; // index in renderer::meshes_
} object;

typedef struct
{
  glm::mat4 model_mat;
  uint16_t material_id;  
} instance_data;

typedef struct
{
  uint  count;
  uint  prim_count;
  uint  first_index;
  uint  base_vertex;
  uint  base_instance;
} draw_elements_indirect_command;

typedef struct
{
  glm::vec4 diffuse;
  glm::vec4 specular;
  glm::vec4 ambient;
  glm::vec4 emissive;
} material_interface;

renderer::renderer()
{
  width_ = 1280;
  height_ = 720;
  samples_ = 1;
  shadow_res_   = 512;
  render_shadows_ = false;
  overlay_enabled_ = false;
  overlay_blend_ = 0.5;
  wireframe_enabled_ = false;
  lens_dist_type_ = LENS_DIST_TYPE_NONE;
  tesselation_factor_ = 1;
}

renderer::~renderer()
{

}

void renderer::set_frame_size(int width, int height)
{
  width_ = width;
  height_ = height;
}

void renderer::set_camera_view_matrix(matrix<3,4> view_mat)
{
  view_mat_ = glm::mat4(1.0);
  for (size_t i=0;i<3;++i) for (size_t j=0;j<4;++j) view_mat_[j][i] = view_mat.at(i,j);
}

void renderer::set_camera_projection(float alpha, float aspect, float znear, float zfar)
{
  cam_alpha_ = alpha;
  cam_aspect_ = aspect;
  cam_near_ = znear;
  cam_far_ = zfar;
}

void renderer::render_shadows(bool s)
{
  render_shadows_ = s;
}

void renderer::set_wireframe(bool enabled)
{
  wireframe_enabled_ = enabled;
}

void renderer::set_lens_distortion_none()
{
  lens_dist_type_ = LENS_DIST_TYPE_NONE;
}

void renderer::set_lens_distortion_poly3(float k1)
{
  lens_dist_type_ = LENS_DIST_TYPE_POLY3;
  lens_dist_coef_.x = k1;
}

void renderer::set_lens_distortion_poly5(float k1, float k2)
{
  lens_dist_type_ = LENS_DIST_TYPE_POLY5;
  lens_dist_coef_.x = k1;
  lens_dist_coef_.y = k2;
}

void renderer::set_lens_distortion_ptlens(float a, float b, float c)
{
  lens_dist_type_ = LENS_DIST_TYPE_PTLENS;
  lens_dist_coef_ = glm::vec3(a,b,c);
}

void renderer::set_lens_distortion(lens_distortion_type type, glm::vec3 coef)
{
  lens_dist_type_ = type;
  lens_dist_coef_ = coef;
}

void renderer::set_tesselation_factor(int tess)
{
  tesselation_factor_ = tess;
}

void renderer::init()
{
  // Buffer creation
  glCreateBuffers(1, &vbo_);
  glCreateBuffers(1, &ibo_);
  glCreateBuffers(1, &instance_);
  glCreateBuffers(1, &command_);

  glCreateBuffers(num_ssbos_, lighting_ssbos_);

  // OpenGL vertex format specification
  glCreateVertexArrays(1, &vao_);
  
  glVertexArrayElementBuffer   (vao_, ibo_);
  // Per-vertex data
  glVertexArrayVertexBuffer    (vao_, 0, vbo_, 0, sizeof(vertex));

  glEnableVertexArrayAttrib    (vao_, 0); // position
  glEnableVertexArrayAttrib    (vao_, 1); // normal
  glEnableVertexArrayAttrib    (vao_, 2); // tangent
  glEnableVertexArrayAttrib    (vao_, 3); // uv

  glVertexArrayAttribFormat    (vao_, 0, 3, GL_FLOAT, GL_FALSE, 0);
  glVertexArrayAttribFormat    (vao_, 1, 3, GL_FLOAT, GL_FALSE, 12);
  glVertexArrayAttribFormat    (vao_, 2, 3, GL_FLOAT, GL_FALSE, 24);
  glVertexArrayAttribFormat    (vao_, 3, 2, GL_FLOAT, GL_FALSE, 36);
  
  glVertexArrayAttribBinding   (vao_, 0, 0);
  glVertexArrayAttribBinding   (vao_, 1, 0);
  glVertexArrayAttribBinding   (vao_, 2, 0);
  glVertexArrayAttribBinding   (vao_, 3, 0);

  // Per-instance data
  glVertexArrayVertexBuffer    (vao_, 1, instance_, 0, sizeof(instance_data));

  glEnableVertexArrayAttrib    (vao_, 4); // model matrix row 0
  glEnableVertexArrayAttrib    (vao_, 5); // model matrix row 1 
  glEnableVertexArrayAttrib    (vao_, 6); // model matrix row 2
  glEnableVertexArrayAttrib    (vao_, 7); // model matrix row 3

  glVertexArrayAttribFormat    (vao_, 4, 4, GL_FLOAT, GL_FALSE, 0);
  glVertexArrayAttribFormat    (vao_, 5, 4, GL_FLOAT, GL_FALSE, 16);
  glVertexArrayAttribFormat    (vao_, 6, 4, GL_FLOAT, GL_FALSE, 32);
  glVertexArrayAttribFormat    (vao_, 7, 4, GL_FLOAT, GL_FALSE, 48);

  glVertexArrayAttribBinding   (vao_, 4, 1);
  glVertexArrayAttribBinding   (vao_, 5, 1);
  glVertexArrayAttribBinding   (vao_, 6, 1);
  glVertexArrayAttribBinding   (vao_, 7, 1);

  glVertexArrayBindingDivisor  (vao_, 1, 1);

  glGetIntegerv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max_anisotropy_);

  init_default_textures();

  // Shadow map blur
  glCreateTextures(GL_TEXTURE_2D, 1, &tex_shadow_blur_);
  glCreateFramebuffers(1, &fbo_shadow_blur_);
  glTextureStorage2D(tex_shadow_blur_, 1, GL_RG32F, 
    shadow_res_, shadow_res_);
  glTextureParameteri(tex_shadow_blur_, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glNamedFramebufferTexture(fbo_shadow_blur_, GL_COLOR_ATTACHMENT0, tex_shadow_blur_, 0);
  
  // Misc.
  glEnable(GL_CULL_FACE);
  glEnable(GL_MULTISAMPLE);

  // Tesselation
  glPatchParameteri(GL_PATCH_VERTICES, 3);
  glm::vec4 outer_level = glm::vec4(tesselation_factor_);
  glm::vec2 inner_level = glm::vec2(tesselation_factor_);
  glPatchParameterfv(GL_PATCH_DEFAULT_OUTER_LEVEL, glm::value_ptr(outer_level));
  glPatchParameterfv(GL_PATCH_DEFAULT_INNER_LEVEL, glm::value_ptr(inner_level));

  // Shader loading
  try
  {
    sha_prepass_.create();
    sha_prepass_.source(GL_VERTEX_SHADER         , "shaders/z_prepass.vert");
    sha_prepass_.source(GL_TESS_EVALUATION_SHADER, "shaders/prepass_tess.tes");
    sha_prepass_.link();

    sha_shadow_.create();
    sha_shadow_.source(GL_VERTEX_SHADER  , "shaders/shadow.vert");
    sha_shadow_.source(GL_FRAGMENT_SHADER, "shaders/shadow.frag");
    sha_shadow_.link();

    sha_lighting_.create();
    sha_lighting_.source(GL_VERTEX_SHADER         , "shaders/lighting.vert");
    sha_lighting_.source(GL_TESS_EVALUATION_SHADER, "shaders/tesselation.tes");
    sha_lighting_.source(GL_FRAGMENT_SHADER       , "shaders/lighting.frag");
    sha_lighting_.link();

    sha_shadow_hblur_.create();
    sha_shadow_hblur_.source(GL_VERTEX_SHADER  , "shaders/deferred.vert");
    sha_shadow_hblur_.source(GL_FRAGMENT_SHADER, "shaders/shadow_hblur.frag");
    sha_shadow_hblur_.link();

    sha_shadow_vblur_.create();
    sha_shadow_vblur_.source(GL_VERTEX_SHADER  , "shaders/deferred.vert");
    sha_shadow_vblur_.source(GL_FRAGMENT_SHADER, "shaders/shadow_vblur.frag");
    sha_shadow_vblur_.link();

    sha_overlay_.create();
    sha_overlay_.source(GL_VERTEX_SHADER  , "shaders/deferred.vert");
    sha_overlay_.source(GL_FRAGMENT_SHADER, "shaders/overlay.frag");
    sha_overlay_.link();

  }
  catch (shader::exception &e)
  {
    throw exception(std::string("Shader loading failure : ") + e.what());
  }
}

void renderer::init_default_textures()
{
  glm::vec4 val[material::TEXTURE_COUNT] = {
    glm::vec4(1,1,1,1),
    glm::vec4(1,1,1,1),
    glm::vec4(0,0,0,1),
    glm::vec4(0.5,0.5,1,1),
    glm::vec4(1,1,1,1),
    glm::vec4(1,1,1,1)
  };

  glCreateTextures(GL_TEXTURE_2D, material::TEXTURE_COUNT, default_textures_);
  for (uint i=0;i<material::TEXTURE_COUNT;++i)
  {
    GLuint tex = default_textures_[i];
    glTextureStorage2D(tex, 1, GL_RGBA8, 1, 1);
    glTextureSubImage2D(tex, 0, 0, 0, 1, 1, GL_RGB, GL_FLOAT, glm::value_ptr(val[i]));
    glTextureParameteri(tex, GL_TEXTURE_MAX_ANISOTROPY_EXT, max_anisotropy_);
  }
}

/**
 * Returns GL format from number of channels
 */
static GLenum get_format(int channels)
{
  switch (channels)
  {
    case 1: return GL_RED;
    case 2: return GL_RG;
    case 3: return GL_RGB;
    case 4: return GL_RGBA;
    default: return GL_NONE;
  }
}

/**
 * Vertically flips an image of given width, height and channels
 */
static void flip_img(uint width, uint height, int channels, uint8_t *img)
{
  uint8_t *temp = new uint8_t[width*channels];
  for (uint i=0;i<height/2;++i)
  {
    uint8_t *loc_1 = img+width*channels*i;
    uint8_t *loc_2 = img+(width*channels)*(height-i-1);
    memcpy(temp , loc_1, width*channels);
    memcpy(loc_1, loc_2, width*channels);
    memcpy(loc_2, temp , width*channels); 
  }
  delete [] temp;
}

void renderer::set_overlay_img(int width, int height, int channels, uint8_t *img)
{
  glCreateTextures(GL_TEXTURE_2D, 1, &tex_overlay_);
  glTextureStorage2D(tex_overlay_, 1, GL_RGB8, width, height);
  flip_img(width, height, channels, img);
  glTextureSubImage2D(tex_overlay_, 0, 0, 0, width, height, get_format(channels), GL_UNSIGNED_BYTE, img);
  glTextureParameteri(tex_overlay_, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  overlay_enabled_ = true;
}

void renderer::set_overlay_blend(float blend)
{
  overlay_blend_ = blend;
}

void renderer::destroy()
{

}

// aiColor3D to glm::vec3
static glm::vec3 toglm(aiColor3D c)
{
  glm::vec3 col;
  col.r = c.r;
  col.g = c.g;
  col.b = c.b;
  return col;
}

// aiVector3D to glm::vec3
static glm::vec3 toglm(aiVector3D v)
{
  glm::vec3 vec;
  vec.x = v.x;
  vec.y = v.y;
  vec.z = v.z;
  return vec;
}

// Converts aiMatrix4x4 to glm::mat4
static glm::mat4 toglm(aiMatrix4x4 m)
{
  glm::mat4 mat;
  aiMatrix4x4 temp = m.Transpose();
  memcpy(&mat, &temp, sizeof(float)*16);
  return mat;
}

/*
 * Gets the path of a texture in a material and stores it in a map associating
 * path and index in renderer::textures_
 */
static int get_texpath(aiMaterial &aimat, aiTextureType type, int layer, 
  std::map<std::string, int> &paths)
{
  aiString temp_p;
  aimat.GetTexture(type, layer, &temp_p);
  std::string path(temp_p.C_Str());
  if (path.empty()) return -1;
  int id = paths.size();
  const auto &ret = paths.insert(std::make_pair(path, id));
  if (ret.second) return id;
  else return ret.first->second;
}

/**
 * Returns the number of mipmaps a texture of a given size should have
 */
static int mipmaps(int width, int height)
{
  int levels = 0;
  while ((width|height) >> levels) ++levels;
  return levels;
}

// Computes radius of light from attenuation factor and light diffuse color
static float radius(glm::vec4 a, glm::vec4 diff)
{
  const float THRESHOLD = 5.0/256.0;
  float max = std::max(diff.x, std::max(diff.y, diff.z));
  return (-a.y + std::sqrt(a.y*a.y - 4*a.z*(a.x-max*(1.0/THRESHOLD))))
        / (2.0*a.z);
}

void renderer::load_lights(const aiScene *scene)
{
  // LIGHT LOADING
  dir_lights_.clear();
  point_lights_.clear();
  spot_lights_.clear();

  uint shadowmap_layers = 0;
  shadow_matrices_.clear();

  for (uint i=0;i<scene->mNumLights;++i)
  {
    aiLight *ailight = scene->mLights[i];
    // Position and direction relative to the scene graph node
    glm::vec4 position  = glm::vec4(toglm(ailight->mPosition) , 1.0);
    glm::vec4 direction = glm::vec4(toglm(ailight->mDirection), 0.0);

    // Scene graph node retrieval
    aiNode *node = scene->mRootNode->FindNode(ailight->mName);
    if (node == NULL) throw exception("Light references nonexistant node");

    // Go up in the tree and apply the parents' transformations
    while (node)
    {
      glm::mat4 mat = toglm(node->mTransformation);
      position  = mat*position;
      direction = mat*direction;
      node = node->mParent;
    }

    // Specific attributes
    if (ailight->mType == aiLightSource_DIRECTIONAL)
    {
      dir_light l;
      l.diffuse_   = glm::vec4(toglm(ailight->mColorDiffuse ),1.0);
      l.ambient_   = glm::vec4(toglm(ailight->mColorAmbient ),1.0);
      l.specular_  = glm::vec4(toglm(ailight->mColorSpecular),1.0);
      l.direction_ = direction;
      // Shadow mapping parameters
      if (render_shadows_)
      {
        l.shadow_index_.x = shadowmap_layers+1;
        float size = 5.0;
        glm::mat4 matrix = glm::ortho(-size,size,-size,size,-size,size)*
          glm::lookAt(glm::vec3(0.0),glm::vec3(l.direction_), glm::vec3(0.0,0.0,1.0));
        shadow_matrices_.insert(shadow_matrices_.begin()+shadowmap_layers, matrix);
        shadowmap_layers += 1;
      }
      dir_lights_.push_back(l);
    }
    else if (ailight->mType == aiLightSource_POINT)
    {
      point_light l;
      l.diffuse_    = glm::vec4(toglm(ailight->mColorDiffuse ),1.0);
      l.ambient_    = glm::vec4(toglm(ailight->mColorAmbient ),1.0);
      l.specular_   = glm::vec4(toglm(ailight->mColorSpecular),1.0);
      l.position_   = position;
      l.attenuation_.x = ailight->mAttenuationConstant;
      l.attenuation_.y = ailight->mAttenuationLinear;
      l.attenuation_.z = ailight->mAttenuationQuadratic;
      l.attenuation_.w = radius(l.attenuation_, l.diffuse_);
      l.shadow_index_.x = 0;
      point_lights_.push_back(l);
    }
    else if (ailight->mType == aiLightSource_SPOT)
    {
      spot_light l;
      l.diffuse_    = glm::vec4(toglm(ailight->mColorDiffuse ),1.0);
      l.ambient_    = glm::vec4(toglm(ailight->mColorAmbient ),1.0);
      l.specular_   = glm::vec4(toglm(ailight->mColorSpecular),1.0);
      l.position_    = position;
      l.attenuation_.x = ailight->mAttenuationConstant;
      l.attenuation_.y = ailight->mAttenuationLinear;
      l.attenuation_.z = ailight->mAttenuationQuadratic;
      l.attenuation_.w = radius(l.attenuation_, l.diffuse_);
      l.direction_   = direction;
      l.inner_angle_ = ailight->mAngleInnerCone;
      l.outer_angle_ = std::min(ailight->mAngleOuterCone, glm::radians(179.f));
      if (render_shadows_)
      {
        l.shadow_index_.x = shadowmap_layers+1;
        glm::mat4 matrix = glm::perspective(l.outer_angle_/2, 1.f, 0.1f,
          l.attenuation_.w)*glm::lookAt(glm::vec3(position), glm::vec3(position+direction),glm::vec3(0,0,1.0));
        shadow_matrices_.insert(shadow_matrices_.begin()+shadowmap_layers, matrix);
        shadowmap_layers += 1;
      }
      spot_lights_.push_back(l);
    }
  }
  if (render_shadows_)
  {
    // Shadow map creation
    glCreateTextures(GL_TEXTURE_2D_ARRAY, 1, &tex_shadow_);
    glTextureStorage3D(tex_shadow_, mipmaps(shadow_res_,shadow_res_), GL_RG32F, 
      shadow_res_, shadow_res_, shadowmap_layers);
    glTextureParameteri(tex_shadow_, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(tex_shadow_, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTextureParameteri(tex_shadow_, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTextureParameteri(tex_shadow_, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTextureParameteri(tex_shadow_, GL_TEXTURE_MAX_ANISOTROPY_EXT, max_anisotropy_);

    // Shadow map views
    tex_view_shadow_.resize(shadowmap_layers);
    glGenTextures(shadowmap_layers, tex_view_shadow_.data());
    for (uint i=0;i<shadowmap_layers;++i)
    {
      glTextureView(tex_view_shadow_.at(i), GL_TEXTURE_2D, tex_shadow_,
        GL_RG32F, 0, 1, i, 1);
    }

    // Shadow fbos
    fbo_shadow_.resize(shadowmap_layers);

    glCreateFramebuffers(shadowmap_layers, fbo_shadow_.data());
    for (uint i=0;i<shadowmap_layers;++i)
    {
      glNamedFramebufferTextureLayer(fbo_shadow_.at(i), GL_COLOR_ATTACHMENT0, tex_shadow_, 0, i);
      GLuint renderbuffer;
      glCreateRenderbuffers(1, &renderbuffer);
      glNamedRenderbufferStorage(renderbuffer, GL_DEPTH_COMPONENT24, shadow_res_, shadow_res_);
      glNamedFramebufferRenderbuffer(fbo_shadow_.at(i), GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderbuffer);
    }
  }
}


void renderer::load_materials(const aiScene *scene, const std::string &directory)
{
  // MATERIAL LOADING
  materials_.clear();
  materials_.reserve(scene->mNumMaterials);

  std::map<std::string, int> texture_paths;

  for (uint i=0;i<scene->mNumMaterials;++i)
  {
    aiMaterial *aimat = scene->mMaterials[i];
    material mat;
    aiColor3D temp;
    aimat->Get(AI_MATKEY_COLOR_DIFFUSE,     temp); mat.diffuse_ =     toglm(temp);
    aimat->Get(AI_MATKEY_COLOR_SPECULAR,    temp); mat.specular_ =    toglm(temp);
    aimat->Get(AI_MATKEY_COLOR_AMBIENT,     temp); mat.ambient_ =     toglm(temp);
    aimat->Get(AI_MATKEY_COLOR_EMISSIVE,    temp); mat.emissive_ =    toglm(temp);
    aimat->Get(AI_MATKEY_OPACITY, mat.opacity_);
    aimat->Get(AI_MATKEY_SHININESS, mat.shininess_);
    aimat->Get(AI_MATKEY_SHININESS_STRENGTH, mat.shininess_strength_);
    // Material texture path
    // Last argument is default index in textures_ if no texture exists
    mat.texture_ids_[material::DIFFUSE_ID  ] = get_texpath(*aimat, aiTextureType_DIFFUSE  , 0, texture_paths);
    mat.texture_ids_[material::SPECULAR_ID ] = get_texpath(*aimat, aiTextureType_SPECULAR , 0, texture_paths);
    mat.texture_ids_[material::EMISSIVE_ID ] = get_texpath(*aimat, aiTextureType_EMISSIVE , 0, texture_paths);
    mat.texture_ids_[material::NORMALS_ID  ] = get_texpath(*aimat, aiTextureType_NORMALS  , 0, texture_paths);
    mat.texture_ids_[material::SHININESS_ID] = get_texpath(*aimat, aiTextureType_SHININESS, 0, texture_paths);
    mat.texture_ids_[material::LIGHTMAP_ID ] = get_texpath(*aimat, aiTextureType_LIGHTMAP , 0, texture_paths);

    materials_.push_back(mat);
  }

  // TEXTURE LOADING
  glDeleteTextures(textures_.size(), textures_.data());
  textures_.resize(texture_paths.size());
  glCreateTextures(GL_TEXTURE_2D, texture_paths.size(), textures_.data());

  for (auto &p : texture_paths)
  {
    std::string path = directory + "/" + p.first;
    int width, height, channels;
    unsigned char *data = stbi_load(
      path.c_str(), 
      &width, &height, 
      &channels, 0);

    int tex_id = textures_.at(p.second);
    GLenum int_format;
    GLenum format;
    switch (channels)
    {
      case 1: int_format = GL_R8;    format = GL_RED;  break;
      case 2: int_format = GL_RG8;   format = GL_RG;   break;
      case 3: int_format = GL_RGB8;  format = GL_RGB;  break;
      case 4: int_format = GL_RGBA8; format = GL_RGBA; break;
    }
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTextureStorage2D(tex_id, 
      mipmaps(width, height), 
      int_format, width, height);
    glTextureSubImage2D(tex_id, 0,
       0, 0,
       width, height,
       format, GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);
    glGenerateTextureMipmap(tex_id);
    glTextureParameteri(tex_id, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  }
}

void renderer::load_meshes(const aiScene *scene)
{
  std::vector<vertex> vertices;
  std::vector<uint32_t> indices;

  // Fullscreen tri for deferred
  vertex v;
  float a = sqrt(2.f)+1;
  v.pos = glm::vec3(-a,-1,0); vertices.push_back(v);
  v.pos = glm::vec3(a,-1,0);  vertices.push_back(v);
  v.pos = glm::vec3(0,a,0);  vertices.push_back(v);
  uint32_t quad[] = {0,1,2};
  indices.assign(quad, quad+3);

  uint vertex_count = 3;
  uint index_count = 3;

  // Actual meshes to load
  meshes_.clear();
  meshes_.reserve(scene->mNumMeshes);

  for (uint i=0;i<scene->mNumMeshes;++i)
  {
    aiMesh *aimesh = scene->mMeshes[i];
    mesh m;
    m.index_first = index_count;
    m.index_count = aimesh->mNumFaces*3;
    m.base_vertex = vertex_count;
    m.material_id = aimesh->mMaterialIndex;
    meshes_.push_back(m);
    uint mesh_vertex_count = aimesh->mNumVertices;

    // attribute loading
    for (uint j=0;j<mesh_vertex_count;++j)
    {
      vertex v;
      aiVector3D pos = aimesh->mVertices[j];
      v.pos.x = pos.x;
      v.pos.y = pos.y;
      v.pos.z = pos.z;
      if (aimesh->mNormals)
      {
        aiVector3D norm = aimesh->mNormals[j];
        v.norm.x = norm.x;
        v.norm.y = norm.y;
        v.norm.z = norm.z;
      }
      if (aimesh->mTangents)
      {
        aiVector3D tang = aimesh->mTangents[j];
        v.tang.x = tang.x;
        v.tang.y = tang.y;
        v.tang.z = tang.z;
      }
      else
      {
        // compute arbitrary tangent
        glm::vec3 axis = glm::vec3(0,0,1);
        if (v.norm == axis) axis = glm::normalize(axis+glm::vec3(0.1,0,1));
        v.tang = glm::normalize(glm::cross(v.norm,axis));
      }
      if (aimesh->HasTextureCoords(0))
      {
        aiVector3D uv = aimesh->mTextureCoords[0][j];
        v.uv.x = uv.x;
        v.uv.y = 1.0-uv.y; // UV flip
      }
      vertices.push_back(v);
    }

    // index loading
    for (uint j=0;j<aimesh->mNumFaces;++j)
    {
      aiFace face = aimesh->mFaces[j];
      for (uint k=0;k<3;++k)
      {
        indices.push_back(face.mIndices[k]);
      }
    }
    vertex_count += mesh_vertex_count;
    index_count  += m.index_count;
  }
  glNamedBufferData(vbo_, vertices.size()*sizeof(vertex),  NULL, GL_STATIC_DRAW);
  glNamedBufferData(ibo_, indices.size()*sizeof(uint32_t), NULL, GL_STATIC_DRAW);
  glNamedBufferSubData(vbo_, 0, vertices.size()*sizeof(vertex), vertices.data());
  glNamedBufferSubData(ibo_, 0, indices.size()*sizeof(uint32_t), indices.data());
}

material_interface assign(material m)
{
  material_interface mi;
  mi.diffuse  = glm::vec4(m.diffuse_ , m.opacity_);
  mi.specular = glm::vec4(m.specular_, m.shininess_);
  mi.ambient  = glm::vec4(m.ambient_ , m.shininess_strength_);
  mi.emissive = glm::vec4(m.emissive_, 0.0);
  return mi;
}

void renderer::load(const std::string &filename)
{
  // import
  Assimp::Importer importer;

  const aiScene *scene = importer.ReadFile(
    filename,
    aiProcess_CalcTangentSpace |
    aiProcess_Triangulate      |
    aiProcess_GenNormals       |
    aiProcess_JoinIdenticalVertices |
    aiProcess_RemoveRedundantMaterials |
    aiProcess_ImproveCacheLocality |
    aiProcess_OptimizeMeshes |
    aiProcess_OptimizeGraph |
    aiProcess_SortByPType);

  if (!scene)
  {
    throw exception(importer.GetErrorString());
  }

  // directory retrieval
  size_t dir_index = filename.rfind("/");
  std::string dir = "./";
  if (dir_index != filename.npos)
    dir += filename.substr(0, dir_index);

  // loading
  load_meshes(scene);
  load_materials(scene, dir);
  load_lights(scene);

  // Fill ssbos for lighting pass
  std::vector<material_interface> mi;
  for (material m : materials_) mi.push_back(assign(m));

  lighting_ssbos_sizes_[0] = materials_.size()*sizeof(material_interface);
  lighting_ssbos_sizes_[1] = dir_lights_.size()  *sizeof(dir_light);
  lighting_ssbos_sizes_[2] = point_lights_.size()*sizeof(point_light);
  lighting_ssbos_sizes_[3] = spot_lights_.size() *sizeof(spot_light);
  lighting_ssbos_sizes_[4] = shadow_matrices_.size() *sizeof(glm::mat4);
  void* data_ptrs[5] = {
    mi.data(), 
    dir_lights_.data(), point_lights_.data(), spot_lights_.data(), 
    shadow_matrices_.data()
  };

  for (uint i=0;i<num_ssbos_;++i)
  {
    glNamedBufferData(lighting_ssbos_[i], lighting_ssbos_sizes_[i], NULL, GL_STATIC_READ);
    glNamedBufferSubData(lighting_ssbos_[i], 0, lighting_ssbos_sizes_[i], data_ptrs[i]);
  }

  // SCENE GRAPH LOADING
  nodes_.clear();

  aiNode *current_node = scene->mRootNode;
  populate_scene_graph(current_node, -1);
}

void renderer::populate_scene_graph(aiNode *an, int parent)
{
  // attributes
  node n;
  n.meshes.assign(an->mMeshes, an->mMeshes+an->mNumMeshes);
  n.transform = toglm(an->mTransformation);
  nodes_.push_back(n);
  int index = nodes_.size()-1;

  // update parent
  if (parent != -1)
  {
    nodes_.at(parent).children.push_back(index);
  }

  // update children
  for (uint i=0;i<an->mNumChildren;++i)
  {
    populate_scene_graph(an->mChildren[i], index);
  }
}

/* Applies transform to each object of the scene and returns a list
 * of objects categorized by material
 */
static void list_scene_objects(
  int node_id, 
  glm::mat4 transform, 
  std::map<uint, std::vector<object>> &objects, 
  const std::vector<node> &scene,
  const std::vector<mesh> &meshes)
{
  node n = scene[node_id];
  transform = n.transform*transform;

  for (int mesh_id : n.meshes)
  {
    object o;
    o.mesh_id = mesh_id;
    o.transform = transform;
    objects[meshes.at(mesh_id).material_id].push_back(o);
  }

  for (int child : n.children)
  {
    list_scene_objects(child, transform, objects, scene, meshes);
  }
}

void renderer::get_texture_ids(material mat, GLuint *ids)
{
  for (uint i=0;i<material::TEXTURE_COUNT;++i)
  {
    int id = mat.texture_ids_[i];
    if (id == -1) ids[i] = default_textures_[i];
    else          ids[i] = textures_.at(id);
  }
}

static void render_deferred_tri()
{
  glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, NULL);
}

void renderer::render()
{
  glm::mat4 proj_mat;
  proj_mat[0][0] = cam_alpha_/cam_aspect_;
  proj_mat[1][1] = cam_alpha_;
  proj_mat[2][2] = (cam_far_+cam_near_)/(cam_near_-cam_far_);
  proj_mat[3][2] = (2*cam_far_*cam_near_)/(cam_near_-cam_far_);
  proj_mat[2][3] = -1;
  proj_mat[3][3] = 0;
  // camera setup
  glm::mat4 proj_inv = glm::inverse(proj_mat);

  // Sort objects by material id
  std::map<uint, std::vector<object>> objects;
  list_scene_objects(0, glm::mat4(1.0), objects, nodes_, meshes_);

  glBindVertexArray(vao_);

  // VISIBLE GEOMETRY

  std::vector<instance_data> instances;
  std::vector<draw_elements_indirect_command> commands;

  int base_instance = 0;

  // Offset and size of command queue for each material (for later use)
  std::map<uint, glm::uvec2> command_offset_count_per_mat;
  uint offset = 0;

  // For each material
  uint num_materials = objects.size();
  for (auto it : objects)
  {
    uint size = 0;
    uint mat_id = it.first;
    // For each object with material
    for (object o : it.second)
    {
      // instance related data
      instance_data inst;
      inst.model_mat = o.transform;
      instances.push_back(inst);

      // mesh render command
      mesh m = meshes_[o.mesh_id];
      draw_elements_indirect_command command;
      command.count = m.index_count;
      command.prim_count = 1;
      command.first_index = m.index_first;
      command.base_vertex = m.base_vertex;
      command.base_instance = base_instance;
      base_instance += command.prim_count;
      commands.push_back(command);
      ++size;
    }
    command_offset_count_per_mat[mat_id] = glm::uvec2(offset,size);
    offset += size;
  }

  // Instance data update
  glNamedBufferData(
    instance_,
    instances.size()*sizeof(instance_data),
    NULL,
    GL_DYNAMIC_DRAW);

  glNamedBufferSubData(
    instance_,
    0, instances.size()*sizeof(instance_data),
    instances.data());

  // Command buffer update
  glNamedBufferData(
    command_,
    commands.size()*sizeof(draw_elements_indirect_command),
    NULL,
    GL_DYNAMIC_READ);

  glNamedBufferSubData(
    command_,
    0, commands.size()*sizeof(draw_elements_indirect_command),
    commands.data());

  glBindBuffer(GL_DRAW_INDIRECT_BUFFER, command_);

  glEnable(GL_DEPTH_TEST);
  glDepthMask(GL_TRUE);
  glDepthFunc(GL_LESS);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  glCullFace(GL_FRONT);

  // SHADOW MAP CALCULATION
  if (render_shadows_)
  {
    sha_shadow_.use();
    float shadow_clear[2] = {1.0, 1.0};
    float shadow_clear_depth = 1.0;
    
    for (uint i=0;i<fbo_shadow_.size();++i)
    {
      GLuint fbo = fbo_shadow_.at(i);
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);
      glClearNamedFramebufferfv(fbo, GL_COLOR, 0, shadow_clear);
      glClearNamedFramebufferfv(fbo, GL_DEPTH, 0, &shadow_clear_depth);
      glViewport(0,0,shadow_res_, shadow_res_);
      glProgramUniformMatrix4fv(sha_shadow_.id(), 0, 1, GL_FALSE, 
        glm::value_ptr(shadow_matrices_.at(i)));
      glClear(GL_DEPTH_BUFFER_BIT);
      glMultiDrawElementsIndirect(
        GL_TRIANGLES,
        GL_UNSIGNED_INT,
        NULL, 
        commands.size(),
        sizeof(draw_elements_indirect_command));
    }

    // SHADOW MAP BLUR
    glDisable(GL_DEPTH_TEST);
    glCullFace(GL_BACK);

    for (uint i=0;i<fbo_shadow_.size();++i)
    {
      sha_shadow_hblur_.use();
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_shadow_blur_);
      glViewport(0,0,shadow_res_,shadow_res_);
      glBindTextureUnit(0, tex_view_shadow_.at(i));
      render_deferred_tri();

      sha_shadow_vblur_.use();
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_shadow_.at(i));
      glViewport(0,0,shadow_res_,shadow_res_);
      glBindTextureUnit(0, tex_shadow_blur_);
      render_deferred_tri();
    }

    glGenerateTextureMipmap(tex_shadow_);
  }

  float angle = tan(1.0/cam_alpha_);
  glm::vec2 fov = glm::vec2(angle*cam_aspect_, angle);

  // Z PREPASS
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  glViewport(0,0,width_,height_);
  glEnable(GL_DEPTH_TEST);
  glCullFace(GL_BACK);
  // Uniforms
  sha_prepass_.use();
  glProgramUniformMatrix4fv(sha_prepass_.id(), 0, 1, GL_FALSE, glm::value_ptr(proj_mat));
  glProgramUniformMatrix4fv(sha_prepass_.id(), 4, 1, GL_FALSE, glm::value_ptr(view_mat_));
  glProgramUniform1i(sha_prepass_.id(), 10, (int)lens_dist_type_);
  glProgramUniform3fv(sha_prepass_.id(), 11, 1, glm::value_ptr(lens_dist_coef_));
  glProgramUniform1f(sha_prepass_.id(), 12, cam_near_);
  // Z-prepass draw
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  glMultiDrawElementsIndirect(
    GL_PATCHES,
    GL_UNSIGNED_INT,
    NULL, 
    commands.size(),
    sizeof(draw_elements_indirect_command));

  // SHADING
  if (wireframe_enabled_) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  glm::mat3 rot_mat(view_mat_);
  glm::vec3 d(view_mat_[3]);
  glm::vec3 camera_pos = -d*rot_mat;

  // Uniforms
  sha_lighting_.use();
  glProgramUniformMatrix4fv(sha_lighting_.id(), 0, 1, GL_FALSE, glm::value_ptr(proj_mat));
  glProgramUniformMatrix4fv(sha_lighting_.id(), 4, 1, GL_FALSE, glm::value_ptr(view_mat_));
  glProgramUniform3fv(sha_lighting_.id(), 9, 1, glm::value_ptr(camera_pos));
  glProgramUniform1i(sha_lighting_.id(), 10, (int)lens_dist_type_);
  glProgramUniform3fv(sha_lighting_.id(), 11, 1, glm::value_ptr(lens_dist_coef_));
  glProgramUniform1f(sha_lighting_.id(), 12, cam_near_);
  // SSBO binds
  std::vector<GLintptr> offsets(num_ssbos_, 0);
  for (uint i=0;i<num_ssbos_;++i)
    if (lighting_ssbos_sizes_[i] > 0)
      glBindBufferRange(GL_SHADER_STORAGE_BUFFER,
        i, lighting_ssbos_[i], offsets.at(i), lighting_ssbos_sizes_[i]);

  // prepass already computed depth values
  glDepthFunc(GL_LEQUAL);
  glDepthMask(GL_FALSE);
  // For each material
  for (auto &it : objects)
  {
    material m = materials_.at(it.first);
    // Uniforms
    glProgramUniform1ui(sha_lighting_.id(), 8, it.first);
    // Textures
    GLuint tex_ids[material::TEXTURE_COUNT+1];
    get_texture_ids(m, tex_ids);
    uint tex_count = material::TEXTURE_COUNT;
    if (render_shadows_)
    {
      tex_ids[material::TEXTURE_COUNT] = tex_shadow_;
      tex_count++;
    }
    
    glBindTextures(0, tex_count, tex_ids);
    // Command submit
    glm::uvec2 command_off_size = command_offset_count_per_mat.at(it.first);
    glMultiDrawElementsIndirect(
      GL_PATCHES,
      GL_UNSIGNED_INT,
      (void*)(command_off_size.x*sizeof(draw_elements_indirect_command)), 
      command_off_size.y,
      sizeof(draw_elements_indirect_command));
  }

  // Overlay
  if (overlay_enabled_)
  {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    sha_overlay_.use();
    glBindTextureUnit(0, tex_overlay_);
    glProgramUniform1f(sha_overlay_.id(), 0, overlay_blend_);
    glProgramUniform2f(sha_overlay_.id(), 1, 1.0/width_, 1.0/height_);
    render_deferred_tri();
    glDisable(GL_BLEND);
  }
}