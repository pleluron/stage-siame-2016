#include "decoder.hpp"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/common.h>
}

#define BUF_SIZE 4096

void decoder::load(const std::string &filename)
{
  AVCodec *codec;

  context = NULL;
  buffer = new uint8_t[BUF_SIZE + FF_INPUT_BUFFER_PADDING_SIZE];

  avcodec_register_all();

  av_init_packet(&avpkt);

  memset(buffer+BUF_SIZE, 0, FF_INPUT_BUFFER_PADDING_SIZE);

  codec = avcodec_find_decoder(AV_CODEC_ID_MPEG4);
  if (!codec) throw std::string("Codec not found");

  context = avcodec_alloc_context3(codec);
  if (!context) throw std::string("Could not allocate video codec context");

  if (codec->capabilities&CODEC_CAP_TRUNCATED) context->flags |= CODEC_FLAG_TRUNCATED;

  if (avcodec_open2(context, codec, NULL) < 0) throw std::string("Could not open codec");

  file = fopen(filename.c_str(), "rb");
  if (!file) throw std::string("Could not open video file");

  frame = av_frame_alloc();
  if (!frame) throw std::string("Could not allocate video frame");

}

bool decoder::next_frame()
{
  avpkt.size = fread(buffer, 1, BUF_SIZE, file);
  if (avpkt.size == 0) return false;

  avpkt.data = buffer;

  int got_frame;
  int len = avcodec_decode_video2(context, frame, &got_frame, &avpkt);
  if (len < 0)
  {
    throw std::string("Error while decoding frame");
    return false;
  }
  if (avpkt.data)
  {
    avpkt.size -= len;
    avpkt.data += len;
  }
  return true;
}

void *decoder::get_frame_data()
{
  return frame->data[0];
}

int decoder::get_frame_width()
{
  return frame->width;
}

int decoder::get_frame_height()
{
  return frame->height;
}

void decoder::close()
{
  avcodec_close(context);
  av_free(context);
  av_frame_free(&frame);
  delete [] buffer;
}