#pragma once

#include <string>

extern "C" {
#include <libavcodec/avcodec.h>
}

class decoder
{
public:
  /**
   * Loads a video file to be processed
   * @param filename file to load
   */
  void load(const std::string &filename);
  /**
   * Gets pointer to pixel data of the current frame
   * @return a pointer to the frame's pixel data
   */
  void* get_frame_data();
  int   get_frame_width();
  int   get_frame_height();

  /**
   * Sets the next frame to be processed
   * @return whether that next frame exists 
   */
  bool next_frame();

  /**
   * Frees the resources used by load()
   */
  void close();

private:
  FILE *file;
  AVCodecContext *context;
  AVFrame *frame;
  AVPacket avpkt;
  uint8_t *buffer;

};