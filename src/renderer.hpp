#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>

#include <vector>

#include <assimp/scene.h>

#include "shader.hpp"
#include "material.hpp"
#include "light.hpp"

#include "matrix_solver.hpp"

typedef unsigned int uint;

typedef struct
{
  uint index_first;
  uint index_count;
  uint base_vertex;
  int material_id;
} mesh;

typedef struct node
{
  std::vector<uint> children;
  std::vector<uint> meshes;
  glm::mat4 transform;
} node;

enum lens_distortion_type
{
  LENS_DIST_TYPE_NONE = 0,
  LENS_DIST_TYPE_POLY3 = 1,
  LENS_DIST_TYPE_POLY5 = 2,
  LENS_DIST_TYPE_PTLENS = 3
};

class renderer
{
public:
  renderer();
  ~renderer();
  void init();
  void destroy();

  /**
   *  Sets the viewport size in pixels
   */
  void set_frame_size(int width, int height);

  /**
   * Shadow mapping parameters
   */
  void set_shadow_res(uint res);
  void render_shadows(bool s);

  /**
   * Camera parameters
   */
  void set_camera_view_matrix(matrix<3,4> view_mat);
  void set_camera_projection(float alpha, float aspect, float near, float far);

  /**
   * 3D model loading
   */
  void load(const std::string &filename);

  /**
   * Overlay image parameters
   */
  void set_overlay_img(int width, int height, int channels, uint8_t *img);
  void set_overlay_blend(float blend);

  /** Debug wireframe mode */
  void set_wireframe(bool enabled);

  void set_lens_distortion_none();
  void set_lens_distortion_poly3(float k1);
  void set_lens_distortion_poly5(float k1, float k2);
  void set_lens_distortion_ptlens(float a, float b, float c);
  void set_lens_distortion(lens_distortion_type type, glm::vec3 coef);

  void set_tesselation_factor(int tess);

  void render();

  class exception : public std::exception
  {
    public:
      exception(const std::string &msg)
      {
        msg_ = msg;
      }
      const char* what() const noexcept
      {
        return msg_.c_str();
      }
    private:
      std::string msg_;
  };

private:
  // Creates default textures for blank materials
  void init_default_textures();
  // Creates own scene graph from assimp scene
  void populate_scene_graph(aiNode *n, int parent);
  // Model loading steps
  void load_lights(const aiScene *scene);
  void load_materials(const aiScene *scene,const std::string &directory);
  void load_meshes(const aiScene *scene);

  // Returns array of texture names to bind
  void get_texture_ids(material mat, GLuint *ids);

  int width_; // window width
  int height_; // window height
  int samples_; // multisample antialiasing samples
  int max_anisotropy_; // max anisotropy ratio for texture sampling

  float cam_alpha_;
  float cam_aspect_;
  float cam_near_;
  float cam_far_;
  glm::mat4 view_mat_;

  GLuint vao_; // vertex array object
  GLuint vbo_; // vertex buffer
  GLuint ibo_; // index buffer
  GLuint instance_; // instance data buffer (model matrix and material id)
  GLuint command_; // command buffer for glMultiDrawIndirect

  // Overlay
  bool overlay_enabled_;
  GLuint tex_overlay_;
  shader sha_overlay_;
  float overlay_blend_;

  // Wireframe debug mode
  bool wireframe_enabled_;

  lens_distortion_type lens_dist_type_; // Type of lens distortion
  glm::vec3 lens_dist_coef_; // Coefficients for lens distortion
  
  const static int num_ssbos_ = 5;
  GLuint lighting_ssbos_[num_ssbos_]; // SSBOs used in lighting pass
  GLintptr lighting_ssbos_sizes_[num_ssbos_]; 

  GLuint tex_shadow_; // Array texture of shadow map
  std::vector<GLuint> tex_view_shadow_; // View texture to each slice of tex_shadow_
  std::vector<GLuint> fbo_shadow_; // FBOs to render depth maps to
  std::vector<glm::mat4> shadow_matrices_; // View matrix for each shadow map
  GLuint fbo_shadow_blur_; // ping pong blur fbos
  GLuint tex_shadow_blur_; // ping pong blur textures

  shader sha_prepass_;  // Z prepass to reject fragments in the lighting pass
  shader sha_shadow_;   // Shadow map generation
  shader sha_shadow_hblur_; // Horizontal blur
  shader sha_shadow_vblur_; // Vertical blur
  shader sha_lighting_; // Lighting computation

  uint shadow_res_; // Resolution of shadow maps
  bool render_shadows_; // Option to render shadows or not

  int tesselation_factor_;

  std::vector<mesh> meshes_;
  std::vector<material> materials_;

  // Lights
  std::vector<dir_light> dir_lights_;
  std::vector<point_light> point_lights_;
  std::vector<spot_light> spot_lights_;

  // Textures
  std::vector<GLuint> textures_;
  GLuint default_textures_[material::TEXTURE_COUNT];
  
  // Scene graph - after loading, the first element is the root node
  std::vector<node> nodes_;

};