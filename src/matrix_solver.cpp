#include "matrix_solver.hpp"
#include <algorithm>
#include <cmath>
#include <cstring>
#include <utility>

#include <iostream>

class pair_reject {};

/* MATRIX DEFINITIONS */

vec<3> cross(const vec<3> &left, const vec<3> &right)
{
  return vec3(
    left.at(1)*right.at(2)-left.at(2)*right.at(1),
    left.at(2)*right.at(0)-left.at(0)*right.at(2),
    left.at(0)*right.at(1)-left.at(1)*right.at(0));
}

vec<2> vec2(double v1, double v2)
{
  vec<2> v;
  v.at(0) = v1;
  v.at(1) = v2;
  return v;
}

vec<3> vec3(double v1, double v2, double v3)
{
  vec<3> v;
  v.at(0) = v1;
  v.at(1) = v2;
  v.at(2) = v3;
  return v;
}

vec<4> vec4(double v1, double v2, double v3, double v4)
{
  vec<4> v;
  v.at(0) = v1;
  v.at(1) = v2;
  v.at(2) = v3;
  v.at(3) = v4;
  return v;
}

matrix<3,3> matrix3(const vec<3> &p1, const vec<3> &p2, const vec<3> &p3)
{
  matrix<3,3> res;
  for (size_t i=0;i<3;++i)
  {
    res.at(0,i) = p1.at(i);
    res.at(1,i) = p2.at(i);
    res.at(2,i) = p3.at(i);
  }
  return res;
}

matrix<3,4> matrix3x4(const matrix<3,3> &m, const vec<3> &v)
{
  matrix<3,4> res;
  for (size_t i=0;i<3;++i)
  {
    for (size_t j=0;j<3;++j)
    {
      res.at(i,j) = m.at(i,j);
    }
    res.at(i,3) = v.at(i);
  }
  return res;
}

/* SOLVER DEFINITIONS */

matrix<3,4> look_at(vec<3> eye, vec<3> center, vec<3> up)
{
  vec<3> dir = normalize(center-eye);
  vec<3> right = normalize(cross(dir, up));
  vec<3> true_up = cross(right, dir);
  matrix<3,4> mat;
  for (int i=0;i<3;++i)
  {
    mat.at(0,i) = right.at(i);
    mat.at(1,i) = true_up.at(i);
    mat.at(2,i) = -dir.at(i);
  }
  mat.at(0,3) = -dot(eye,right);
  mat.at(1,3) = -dot(eye,true_up);
  mat.at(2,3) = dot(eye,dir);
  return mat;
}

matrix<4,4> perspective(float alpha, float aspect, float near, float far)
{
  matrix<4,4> mat;

  mat.at(0,0) = alpha/aspect;
  mat.at(1,1) = alpha;
  mat.at(2,2) = -(far+near)/(far-near);
  mat.at(3,2) = -1;
  mat.at(2,3) = -(2*far*near)/(far-near);
  mat.at(3,3) = 0;

  return mat;
}

// Returns the alpha factor (2*focal*crop_factor/sensor height) from a list of points
static float get_alpha(vec<3> view_point, vec<2> ndc_point, float aspect, float deviation)
{
  if (fabs(ndc_point.at(0,0)*view_point.at(1,0)*aspect - 
          ndc_point.at(1,0)*view_point.at(0,0))>deviation) throw pair_reject();

  float alpha1 = (ndc_point.at(0,0)*view_point.at(2,0)*aspect)/view_point.at(0,0);
  float alpha2 = (ndc_point.at(1,0)*view_point.at(2,0))/view_point.at(1,0);

  return (alpha1+alpha2)*0.5;
}

float mean(std::vector<float> array)
{
  float m = 0.0;
  for (float f : array) m += f;
  return m/array.size();
}

float variance(std::vector<float> array, float mean)
{
  float v = 0.0;
  for (float f : array) v += f*f;
  return v/array.size() - mean*mean;
}

float median(std::vector<float> array)
{
  std::sort(array.begin(), array.end());
  int n = array.size();
  if (n%2) return array.at(n/2+1);
  else return (array.at(n/2)+array.at(n/2+1))*0.5;
}

float solve_perspective(
  matrix<3,4> view_matrix,
  std::vector<vec<3>> world_points,
  std::vector<vec<2>> ndc_points,
  float aspect,
  float deviation)
{
  if (world_points.size() != ndc_points.size())
    throw solver_no_solution();

  // alphas that passed
  std::vector<float> alphas;
  for (unsigned int i=0;i<world_points.size();++i)
  {
    vec<3> world_point = world_points.at(i);
    vec<4> w_point; for (int i=0;i<3;++i) w_point.at(i,0) = world_point.at(i,0); w_point.at(3,0) = 1.0;
    vec<3> view_point = view_matrix * w_point;
    vec<2> ndc_point = ndc_points.at(i);
    try
    {
      alphas.push_back(get_alpha(view_point, ndc_point, aspect, deviation));
    }
    catch (...) {}
  }
  if (alphas.empty()) throw solver_no_solution();
  float alpha = mean(alphas);
  return alpha;
}

// View space for a marker in image size
vec<3> marker_screen_to_view_space(marker m, float alpha, float aspect, int height, float marker_size)
{
  // The only time thales' theorem served me
  float z = (0.5*alpha*marker_size)/(m.size/(float)height);
  vec<2> clip = vec2(2*m.pos.at(0)/(height*aspect)-1, 1-2*m.pos.at(1)/height);
  vec<3> v = vec3(clip.at(0)*z*aspect/(alpha), clip.at(1)*z/(alpha),-z);
  return v;
}

// Generates all combinations of R numbers in a set of N numbers
template <class T>
std::vector<std::vector<T>> combinations(const std::vector<T> &vec, size_t r)
{
  std::vector<std::vector<T>> result;
  size_t n = vec.size();
  if (n<r) return result;
  std::vector<bool> v(n);
  std::fill(v.begin() + n - r, v.end(), true);
  do
  {
    std::vector<T> c;
    for (size_t i=0; i<n;++i) {
        if (v[i]) c.push_back(vec.at(i));
    }
    result.push_back(c);
  } while (std::next_permutation(v.begin(), v.end()));
  return result;
}

template <class T>
std::vector<std::vector<T>> permutations(const std::vector<T> &vec)
{
  std::vector<std::vector<T>> res;
  std::vector<int> p(vec.size());
  for (size_t i=0;i<vec.size();++i) p[i] = i;
  do
  {
    std::vector<T> current(p.size());
    for (size_t i=0;i<p.size();++i) current[i] = vec.at(p[i]);
    res.push_back(current);
  }
  while (std::next_permutation(p.begin(),p.end()));
  return res;
}

/**
 * Computes all distances in the given set of points.
 */
std::vector<float> distances(const std::vector<vec<3>> &points)
{
  std::vector<float> res;
  for (size_t j=0;j<points.size()-1;++j)
  {
    for (size_t k=j+1;k<points.size();++k)
    {
      res.push_back(length(points[j]-points[k]));
    }
  }
  return res;
}

/**
 * Associates points in world space with points in view space, using the fact that
 * distances between points are the same in world and view space
 */
std::vector<std::pair<vec<3>,vec<3>>> get_pairs(
  const std::vector<vec<3>> &world_points, 
  const std::vector<vec<3>> &view_points)
{
  const float MAX_DEVIATION = 4e-1;
  size_t n = world_points.size();
  size_t r = view_points.size();

  if (n<=r)
  {
    std::vector<float> world_dists = distances(world_points);
    auto c = combinations(view_points,n);
    float min_deviation = 10000.0;
    std::vector<vec<3>> correct_comb;
    for (auto view_comb : c)
    {
      for (auto view_perm : permutations(view_comb))
      {
        std::vector<float> view_dists = distances(view_perm);
        bool correct_perm = true;
        float deviation = 0.0;
        for (size_t i=0;i<world_dists.size();++i)
        {
          float diff = fabs(world_dists[i]-view_dists[i]);
          if (diff > MAX_DEVIATION)
          {
            correct_perm = false;
            break;
          }
          else deviation += diff;
        }
        if (correct_perm && min_deviation > deviation)
        {
          min_deviation = deviation;
          correct_comb = view_perm;
        }
      }
    }
    std::vector<std::pair<vec<3>,vec<3>>> result;
    for (size_t i=0;i<world_points.size();++i)
    {
      result.push_back(std::make_pair(world_points.at(i),correct_comb.at(i)));
    }
    return result;
  }
  else
  {
    std::vector<float> view_dists = distances(view_points);
    auto c = combinations(world_points, r);
    float min_deviation = 10000.0;
    std::vector<vec<3>> correct_comb;
    for (auto world_comb : c)
    {
      for (auto world_perm : permutations(world_comb))
      {
        std::vector<float> world_dists = distances(world_perm);
        bool correct_perm = true;
        float deviation = 0.0;
        for (size_t i=0;i<world_dists.size();++i)
        {
          float diff = fabs(world_dists[i]-view_dists[i]);
          if (diff > MAX_DEVIATION)
          {
            correct_perm = false;
            break;
          }
          else deviation += diff;
        }
        if (correct_perm && min_deviation > deviation)
        {
          min_deviation = deviation;
          correct_comb = world_perm;
        }
      }
    }
    std::vector<std::pair<vec<3>,vec<3>>> result;
    for (size_t i=0;i<view_points.size();++i)
    {
      result.push_back(std::make_pair(correct_comb.at(i),view_points.at(i)));
    }
    return result;
  }
}

/**
 * Unzips a vector of pairs into two vectors
 * Example : [[1,2],[3,4],[5,6]] -> [1,3,5] and [2,4,6] 
 */
template<class T, class S>
void unzip(const std::vector<std::pair<T,S>> &list, std::vector<T> &first, std::vector<S> &second)
{
  first.clear();
  second.clear();

  for (auto p : list)
  {
    first.push_back(p.first);
    second.push_back(p.second);
  }
}

/**
 * Computes the orthographic matrix from 3 points
 */
matrix<3,3> rotation_matrix(const std::vector<vec<3>> &points)
{
  vec<3> p1 = normalize(points[1]-points[0]);
  vec<3> p2 = normalize(points[2]-points[0]);
  vec<3> c = normalize(cross(p2,p1));
  p2 = normalize(cross(c,p1));
  return transpose(matrix3(p1,p2,c));
}

matrix<3,3> average(const std::vector<matrix<3,3>> &mat)
{
  matrix<3,3> result;
  vec<3> v[3];
  for (auto m : mat)
  {
    for (size_t i=0;i<3;++i)
      v[i] = v[i] + vec3(m.at(0,i), m.at(1,i),m.at(2,i));
  }
  return transpose(matrix3(normalize(v[0]), normalize(v[1]), normalize(v[2])));
}

matrix<3,4> solve_view(
  std::vector<vec<3>> world_points,
  std::vector<marker> screen_points, // image markers as 2D point + size
  float alpha,
  float aspect,
  int height,
  float marker_size)
{
  size_t num_ws = world_points.size();
  size_t num_screen = screen_points.size();

  if (num_screen < 3) throw solver_no_solution();

  // Convert screen space to view space
  std::vector<vec<3>> view_points;
  for (marker m : screen_points)
    view_points.push_back(marker_screen_to_view_space(m, alpha, aspect, height, marker_size));
  
  // Detect pairs
  auto pairs = get_pairs(world_points, view_points);

  for (auto p : pairs)
  {
    //std::cout << p.first.to_string() << " : " << p.second.to_string() << std::endl;
  }

  std::vector<matrix<3,3>> orientation_matrices;

  matrix<3,3> right_hand = identity<3>();
  right_hand.at(2,2) = -1;

  // For each combination of 3 points
  for (auto comb : combinations(pairs, 3))
  {
    // Compute the unit matrix in each space
    std::vector<vec<3>> w, v;
    unzip(comb, w,v);
    matrix<3,3> world_rot = rotation_matrix(w);
    matrix<3,3> view_rot  = rotation_matrix(v);
    matrix<3,3> view_ori = view_rot*transpose(world_rot);
    orientation_matrices.push_back(view_ori);
  }

  vec<3> camera_pos;
  for (auto ori : orientation_matrices)
  {
    vec<3> pos;
    for (auto p : pairs)
    {
      pos += transpose(-transpose(p.second - ori*p.first)*ori);
    }
    camera_pos += 1.0/pairs.size() * pos;
  }
  camera_pos = 1.0/orientation_matrices.size() * camera_pos;
  std::cout << "Camera pos = " << camera_pos.to_string() << std::endl;

  matrix<3,3> avg = average(orientation_matrices);
  matrix<3,4> res = matrix3x4(avg, -avg*camera_pos);
  return res;
}