#include "image_analysis.hpp"

#include <opencv2/opencv.hpp>
#include <vector>

using namespace cv;

marker to_marker(RotatedRect r)
{
  marker m;
  m.pos.at(0) = r.center.x;
  m.pos.at(1) = r.center.y;
  m.size = std::max(r.size.width,r.size.height);
  m.angle = r.angle;
  return m;
}

std::vector<marker> detect_markers(Mat img, float thres, float min_size, float max_size)
{
  std::vector<marker> markers;
  Mat ping,pong;

  // Custom conversion to grayscale
  float luma_data[3] = {0.55,0.0,0.45};
  Mat_<float> luma(1,3,luma_data);

  cv::transform(img, ping, luma);
  //cvtColor(img, ping, CV_BGR2GRAY);

  std::vector<std::vector<Point>> contours;
  std::vector<Vec4i> hierarchy;

  // Blur and thresholding
  GaussianBlur(ping,pong, Size(5,5), 1.0);
  threshold(pong, ping, thres, 255, THRESH_BINARY);

  // Find contours and put them in a two-level hierarchy
  findContours(ping, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, Point(0,0));

  for (size_t i=0;i<contours.size();++i)
  {
    Mat contour = Mat(contours[i]);
    // Discard whole-screen quad and contours above threshold
    if (contours[i].size() > 5 && hierarchy[i][3]!=-1)
    {
      float area = contourArea(contours[i]);
      RotatedRect ellipse = fitEllipse(contour);
      float ellipse_area = ellipse.size.height*ellipse.size.width*M_PI/4;
      // Compare blob size vs. ellipse size
      if (ellipse_area < area*1.15)
      {
        marker m = to_marker(ellipse);
        if (m.size >= min_size && m.size <= max_size)
          markers.push_back(m);
      }
    }
  }
  return markers;
}