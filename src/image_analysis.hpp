#pragma once

#include "matrix_solver.hpp"

#include <opencv2/opencv.hpp>
#include <vector>

std::vector<marker> detect_markers(cv::Mat img, float thres=128, float min_size=0.0, float max_size=500.0);