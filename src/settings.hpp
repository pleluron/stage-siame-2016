#pragma once

#include "matrix_solver.hpp"
#include <string>

typedef struct 
{
  // 3d model file path
  std::string model_path;
  // video file path
  std::string video_path;
  // output video file path
  std::string output_path;

  // RENDERING PARAMETERS
  int height;
  int samples;

  // POSITION & ORIENTATION PARAMETERS
  vec<3> camera_pos;
  vec<3> camera_lookat;
  vec<3> camera_up;

  // FOV PARAMETERS
  float focal_length;
  float crop_factor;
  float sensor_height;

  // DISTORTION PARAMETERS
  int distortion_type; // 0 for no distortion, 1 for poly3, 2 for poly5, 3 for ptlens
  vec<3> distortion_coef; // k1 for poly3, k1&k2 for poly5, a b c for ptlens

  // MARKER INFO
  float marker_size;
  int marker_threshold;
  std::vector<vec<3>> markers;
} settings;

class settings_exception : std::exception {};

void load_settings(const std::string &filename, settings &s);