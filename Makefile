CC=gcc
CXX=g++
MKDIR_P = mkdir -p

PREREQ_DIR=obj/prerequisites
OBJ_DIR=obj
SRC_DIRS=src tests src/thirdparty
OUTPUT_DIR=bin

CFLAGS=-Wall -Iinclude/
CXXFLAGS=-W -std=c++11 -D_USE_MATH_DEFINES -Iinclude/

LIBS=assimp glfw3 opencv

INCLUDE=`pkg-config glm $(LIBS) --cflags`
LDFLAGS=-pthread `pkg-config $(LIBS) --libs`

ifneq ($(OS),Windows_NT)
	LDFLAGS+= -ldl
endif

rules=test1 test2 matrix_test opencv_test tracker_video

# DEPENDENCIES FOR BINARIES
test1=glad
test2=glad renderer matrix_solver
still_image=glad renderer settings
overlay=glad renderer settings
matrix_test=matrix_solver
opencv_test=image_analysis
tracker_video=matrix_solver image_analysis settings renderer glad
# DEPENDENCIES FOR SRC FILES
renderer=shader
settings=shaun sweeper

# DEBUG
DEBUG ?= 1

deps = $(1) $(foreach name,$($(1)),$(call deps,$(name)))

ifeq ($(DEBUG),1)
define make_rule
$(2): $(1)
$(1): $(foreach dep,$(call deps,$(2)),$(OBJ_DIR)/$(dep).o)
	@echo Building debug binary $$@...
	@$(CXX) -g $$^ -o $$@ $(LDFLAGS)
endef
else
define make_rule
$(2): $(1)
$(1): $(foreach dep,$(call deps,$(2)),$(OBJ_DIR)/$(dep).o)
	@echo Building release binary $$@...
	@$(CXX) $$^ -o $$@ $(LDFLAGS)
endef
endif

define make_prerequisites
$(PREREQ_DIR)/%.d: $(1)/%.c*
	@echo Building prerequisite $$*.d...
	@$(MKDIR_P) $(PREREQ_DIR)
	@set -e
	@rm -f $$@
	@$(CXX) -MM -MG $$< -std=c++11 > $$@.$$$$
	@sed 's,\($$*\)\.o[ :]*,obj/\1.o $$@ : ,g' < $$@.$$$$ > $$@
	@rm -f $$@.$$$$
endef

$(foreach dir,$(SRC_DIRS),$(eval $(call make_prerequisites,$(dir))))

all: $(rules)

# generate rules for each binary
$(foreach name,$(rules),$(eval $(call make_rule,$(OUTPUT_DIR)/$(name),$(name))))

define gen_rule_cpp
obj/%.o: $(1)/%.cpp $(PREREQ_DIR)/%.d
	@echo Building cpp source $$<...
	$(eval $(call include $(PREREQ_DIR)/$$*.d))
	@$(CXX) -g -o $$@ -c $$< $(CXXFLAGS)
endef

define gen_rule_c
obj/%.o: $(1)/%.c $(PREREQ_DIR)/%.d
	@echo Building c source $$<...
	$(eval $(call include $(PREREQ_DIR)/$$*.d))
	@$(CC) -o $$@ -c $$< $(CFLAGS)
endef

$(foreach dir,$(SRC_DIRS),$(eval $(call gen_rule_cpp,$(dir))))
$(foreach dir,$(SRC_DIRS),$(eval $(call gen_rule_c,$(dir))))

clean:
	rm -f obj/*.o;
	rm -f $(PREREQ_DIR)/*.d

.PHONY: all clean $(rules)

.PRECIOUS: $(PREREQ_DIR)/%.d
