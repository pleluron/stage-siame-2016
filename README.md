=== RENDERING PIPELINE ===

Render all visible objects to a G-Buffer:
  - Attachment 0 : RGBA16 for UVs (rg) and Depth gradients (ba)
  - Attachment 1 : RGB10_A2 for tangent vector
  - Attachment 2 : RGBA16 for UV gradients
  - Attachment 3 : R16UI for material id

Split the screen into 16x16 tiles

In a compute shader, for each tile:
  - Make a list of materials in that tile
  - Compute lights intersecting tile

Convert to tile per material (CPU side)

For each material :
  - Bind needed textures
  - Run a compute shader for each tile to compute final image (lighting, shadows...)

Blit image to screen
