// Crée une texture
glGenTextures(1, &tex_id);
// Alloue une texture RGB de 256x256
glBindTexture(GL_TEXTURE_2D, tex_id);
glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB, 256, 256);

// Crée une texture, mais le type est définitif
glCreateTextures(GL_TEXTURE_2D, 1, &tex_id);
// Alloue la texture directement sans bind, et sans préciser le type
glTextureStorage2D(tex_id, 1, GL_RGB, 256, 256);

foreach (material)
foreach (object)
{
  writeData(object);
  glDrawElements(
    GL_TRIANGLES, 
    object->indexCount, 
    GL_UNSIGNED_INT,
    NULL);
}

foreach (material)
foreach (object)
{
  addToQueue(queue, object);
}
glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, queue, ...);

//Paramètres de rendu
width=960
height=540
samples=16
//Paramètres de la caméra
camera_pos=(4.52,-4.71,1.57)
camera_lookat=(2.69,-2.015,0.75)
camera_up=(0.0,0.0,1.0)
//Distance focale de la caméra
focal_length=20
//Crop factor propre à la caméra
crop_factor=2.1875

// Exemple de distorsion de type PTLens
distorsion_type=PTLENS
a=0.024
b=-0.018
c=0.005

template <int M, int N>
class matrix
{
public:
  matrix();
  float &at(int i, int j);
  std::string to_string();
  matrix<M-1,N-1> submatrix(int i, int j);
private:
  float values[M][N];
};

template<int M>
matrix<M,M> inverse(matrix<M,M> mat) { /* ... */ };

template<int M, int N>
matrix<N,M> tranpose(matrix<M,N> mat) { /* ... */ };

matrix<4,3> mat;

template <int M>
class determinant
{
public:
  float value(matrix<M,M> mat)
  {
    float det = 0.0;
    for (int i=0;i<M;++i)
    {
      det += ((i%2)*-2+1) * mat.at(0,i) *
        determinant<M-1>().value(mat.submatrix(0, i));
    }
    return det;
  }
};

template <>
class determinant<1>
{
public:
  float value(matrix<1,1> mat)
  {
    return mat.at(0,0);
  }
};